import {Container , Button ,Card ,Row,Col , Image} from 'react-bootstrap'
import {TextField, FormControl , TextareaAutosize} from '@material-ui/core'
import SimpleSelect from '../HomeInquire '
import Custom1 from '../asset/image/Custom1.png'
import Custom2 from '../asset/image/Custom2.png'
import Custom3 from '../asset/image/Custom3.png'
import { Route, Router } from 'react-router-dom';
import FooterLinks from '../FooterLink';
import FacebookIcon from '@material-ui/icons/Facebook';
import TwitterIcon from '@material-ui/icons/Twitter';
import InstagramIcon from '@material-ui/icons/Instagram';
import './Custom.css'


function Custom() {
    function Qoute(){
        alert("SORRY :  This Service Is Unavailable At This Time")
        }
        function  NewsLetters() {
            alert("SORRY :  This Service Is Unavailable At This Time")
          }
    return(
        <div style={{ backgroundColor: "	 #eee2e2" }}>
        <Container>
          <Row className="OfficeMovingMain">
            <Col md={6} className="mt-5">
              <h1 className="OfficeMovingHeading">Custom Clearance Agent</h1>
              <ul>
                <li>Day and Night Service</li>
                <li>Professional Agent</li>
                <li>24/7 Customer Support</li>
              </ul>
              <h3 className="OfficeMovingHeading"> +92-3112755732</h3>
            </Col>
            <Col md={6} className="mt-5">
              <Row>
                <Col md={5}>
                  <TextField id="standard-basic" label="First Name" type="text" />
                </Col>
                <Col md={5}>
                  <TextField id="standard-basic" label="Last Name" className="mb-3" />
                </Col>
      
                <Col md={5}>
                  <TextField id="standard-basic" label="Email" type="mail" />
                </Col>
                <Col md={5}>
                  <TextField
                    id="standard-basic"
                    label="Phone Number"
                    type="text"
                    className="mb-3"
                  />
                </Col>
      
                <Col md={5}>
                  <TextField id="standard-basic" label="City" />
                </Col>
                <Col md={5}>
                  <TextField id="standard-basic" label="Country" className="mb-3" />
                </Col>
                <Col md={5} className="mb-3">
                  <SimpleSelect />
                </Col>
              </Row>
              <Row>
                <Col md={5}>
                  <Button
                    type="submit"
                    onClick={() => Qoute()}
                    className="OfficeMovingButton"
                  >
                    Submit Form
                  </Button>
                </Col>{" "}
                <br /> <br />
                <Col md={5}>
                  <Button
                    type="reset"
                    onClick={() => Qoute()}
                    className="OfficeMovingButton"
                  >
                    Reset Form
                  </Button>
                </Col>
              </Row>
            </Col>
          </Row>
          <Row className="mt-3">
            <Col md={12}>
              <h1 className="CustomHeading">Custom Clearance Services</h1>
              <p className="CustomText">
                Rapid Movers Lahore feels pleasure as one of the best customs
                clearance broker, freight forwarding, in Pakistan’s different cities.
                We have in-house Import Freight forwarding Division guaranteeing top
                quality of service from packing till delivery to your doorstep.
              </p>
              <p className="CustomText">
                Our team moved your all consignments swiftly and efficiently with
                proper documentation. Having 10+ years of excellence, we know our
                customer’s demands, our offices across Pakistan focus on customs
                regulations and internal agreement details and ensure special tariff
                and all management rules.
              </p>
              <p className="CustomText">
                We are experts in import & export customs clearance for household
                items, baggage, & sea shipments. Custom Clearance agency is defined as
                a customs brokerage in Pakistan. We offer fantastic customs agent
                services to our customers to ensure that your luggage and other moving
                products are packed safely & cleared from customs at very affordable
                rates to our clients on time.{" "}
              </p>
            </Col>
            <Col md={6} className="mt-5">
              <h1 className="CustomHeading">Certified Custom Clearing Agency</h1>
              <p className="CustomText">
                Rapid Movers and packers are professional, independent customs
                clearing agents and complete many projects daily with excellence. As a
                licensed & certified customs clearing agency with the national and
                international partnership with best shipping lines, warehouses and
                customs experts, we take care of Customs clearance nationwide at all
                Pakistani ports with extra care of your belongings.{" "}
              </p>
              <p className="CustomText">
                If you move from Pakistan to America and have heavy belongings, many
                airlines’ restrictions for carrying luggage are costly. We provide the
                hassle-free and cost-effective moving solution with full documentation
                of custom clearing.{" "}
              </p>
            </Col>
            <Col md={6}>
              <img as={Image} src={Custom1} className="CustomImage" />
            </Col>
            <Col md={6}>
              <img as={Image} src={Custom2} className="CustomImage mt-5" />
            </Col>
            <Col md={6} className=" mt-5">
              <h1 className="CustomHeading">
                Import and Export Custom Clearing Services
              </h1>
              <p className="CustomText">
                Solving import and export problems will upgrade anyone’s trading
                business in today’s world. Everyone needs a business partner who
                delivers your items on-premises, provide low-cost business solutions,
                & respond to your business needs.
              </p>
              <p className="CustomText">
                We are helping many businesses to grow because we are providing the
                best import and export solutions. We work hassle-free to offer not
                just any solution but the right solution for your business.
              </p>
              <p className="CustomText">
                When a shipment of goods or luggage is released into Pakistan,
                specific needs are set by Pakistan’s customs agencies. All the
                necessary documentation must be correctly completed and accurately
                submitted. Rapid Packers & Movers customs agent’s staff are excellent
                in their knowledge of customs import laws, best in the care and
                attention to each customer’s import requirements.
              </p>
            </Col>
            <Col md={6} className="mt-5">
              <h1 className="CustomHeading">
                {" "}
                Industrial and Commercial Import Services
              </h1>
              <p className="CustomText">
                Our professional staff manage both industrial and commercial Import
                Shipments at different ports of Pakistan. Our customs clearance cost
                is meagre compared to other custom companies, and we delivered your
                bulky items with the legal certificate.
              </p>
              <p className="CustomText">
                Most of our ship freight are delivered within 24 hours of their
                arrival. Our Customs Clearance professionals have over ten years’
                experience in assuring the complete care and giving your best tips to
                save cost on customs clearance your goods. We provide customs
                clearance services with full documentation at very cheap rates.
              </p>
              <p className="CustomText">
                We have collaborations with famous Pakistani ports to import, and
                export businesses with complete documentation of goods, and ports are
                mentioned here:
              </p>
              <h6>
                Karachi Port, Port Qasim, Karachi Airport, Lahore Dry port, Lahore
                Airport, Islamabad Airport, Islamabad Dry Port
              </h6>
            </Col>
            <Col md={6}>
              <img as={Image} src={Custom3} className="CustomImage mt-5" />
            </Col>
          </Row>
        </Container>
      
        <footer className="Footer1">
          <Container>
            <Row className="Footer">
              <Col md={3}>
                <Image src="https://ishfaqmovers.com/wp-content/uploads/2018/10/mo-white.png"></Image>
                <h6 className="HomeFooterText">
                  ISHFAQ MOVERS has been one of the leading packers & Movers in
                  Karachi-Pakistan for more than 10 years. We deliver a broad range of
                  targeted national and international moving services for home, house,
                  villa, apartments, offices as well as relocation solutions for every
                  purpose.
                </h6>
              </Col>
              <Col md={3} className="mt-3">
                <h3>QUICK LINKS</h3>
      
                <p>
                  <FooterLinks />
                </p>
      
                <Route path="/about">
                  <FooterLinks />
                </Route>
                <Route path="/contact">
                  <FooterLinks />
                </Route>
              </Col>
              <Col md={3} className="mt-3">
                <h3>OUR SERVICES</h3>
                <p>
                  <FooterLinks />
                </p>
      
                <Route path="/about">
                  <FooterLinks />
                </Route>
                <Route path="/contact">
                  <FooterLinks />
                </Route>
              </Col>
              <Col md={3} className="mt-">
                <h3>NEWSLETTER SIGNUP</h3>
                <h6>No spam, unsubscribe at any time.</h6>
                <TextField id="standard-basic" label="Email" type="mail" />
                <br />
                <Button onClick={() => NewsLetters()} className="HomeButton mt-2 ">
                  Subscribe
                </Button>
                <br />
                <h5 className="mt-2 ">Contact On Social Media</h5>
                <a href="#">
                  <FacebookIcon></FacebookIcon>
                </a>
                <a href="#">
                  <TwitterIcon></TwitterIcon>
                </a>
                <a href="#">
                  <InstagramIcon></InstagramIcon>
                </a>
              </Col>
            </Row>
          </Container>
          <hr />
        </footer>
      </div>
      
    )
    
}
export default Custom;