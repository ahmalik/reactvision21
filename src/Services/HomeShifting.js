import {Container , Button ,Card ,Row,Col , Image} from 'react-bootstrap'
import {TextField, FormControl , TextareaAutosize} from '@material-ui/core'
import SimpleSelect from '../HomeInquire '
import FooterLinks from '../FooterLink';
import { Route, Router } from 'react-router-dom';
import HomeShifting1 from '../asset/image/Home Shifting1.jpg'
import HomeShifting2 from '../asset/image/Home Shifting2.jpg'
import HomeShifting3 from '../asset/image/Home Shifting3.png'
import HomeShifting4 from '../asset/image/Home Shifting4.png'
import HomeShifting5 from '../asset/image/Home Shifting5.png'
import HomeShifting6 from '../asset/image/Home Shifting6.jpg'
import './HomeShifting.css' 
function HomeShifting(){
    function Qoute(){
        alert("SORRY :  This Service Is Unavailable At This Time")
        }
    return(
        <div style={{backgroundColor:"	 #eee2e2"}}>
            <Container  >
                <Row className="HomeShiftingMain">
                    <Col md={6} className="mt-5">
                    <h1 className=" HomeShiftingHeading">AHR MOVERS Home Shifting Services</h1>
                    <ul>
                        <li>Day and Night Shifting Service</li>
                        <li>Professional Team</li>
                        <li>24/7 Customer Support</li>
                      
                    </ul>
                    <h3 className=" HomeShiftingHeading">  +92-3112755732</h3>
                    </Col>
                    <Col md={6} className="mt-5">
                    <Row>
                                <Col md={5}><TextField id="standard-basic" label="First Name" type="text" /></Col>
                                <Col md={5}><TextField id="standard-basic" label="Last Name" className="mb-3" /></Col>
                              
                                <Col md={5}><TextField id="standard-basic" label="Email" type="mail" /></Col>
                                <Col md={5}><TextField id="standard-basic" label="Phone Number" type="text"className="mb-3" /></Col>
                                
                                <Col md={5}><TextField id="standard-basic" label="Moving To"  /></Col>
                                <Col md={5}><TextField id="standard-basic" label="Moving From"  className="mb-3" /></Col>
                                <Col md={5} className="mb-3"><SimpleSelect /></Col>
                                </Row>
                                <Row>
                                  <Col md={5}><Button type="submit" onClick={()=>Qoute()}   className="HomeShiftingButton"  >Submit Form</Button></Col> <br/> <br/>
                                  <Col md={5}><Button type="reset" onClick={()=>Qoute()}  className="HomeShiftingButton">Reset Form</Button></Col>
                                </Row>
                    </Col>
                </Row>
            </Container>
            <br/>
            <Container>
            <h3 className="HomeShiftingHeading mt-3">Hire A Professional Company For Home Shifting
        </h3>
        <p>A Person may need to transfer his house due to several reasons for his education, Business purpose, or any
            other personal reasons. Rapid Packers and movers carry out every stage of shifting with perfection. We
            heartily understand and value the emotions of our customers, and we use premium grade packing material.
            Rapid Packers and movers is a well-founded name in packing and moving. We are an excellent choice for Home
            Shifting.</p>

        <p>We heartily care about what our customer is demanding from us. When you place your order, it is all up to us,
            and you don’t need to have any worries after that. When we get an order, we plan to deliver at a specific
            date and time. The best thing about our services is that it is affordable for those who are looking for it.
        </p>
        <p>No doubt that Home Shifting is a massive task for people who need to transfer their homes. But we are here to
            make sure it becomes easy for you. We guarantee you that your items will be protected with the use of
            high-quality packing materials, such as: Special Wrapping Papers, Double thickness cartoon, Best Plastic
            wraps.</p>
        <p>Why miss the chance to call us today and get the best services for Home moving. We are mainly providing Home
            Shifting Service in the four cities of Pakistan that are: Lahore, Islamabad, Rawalpindi.</p>
        <p>We use different kinds of techniques and packing material for different types of furniture at home like,

        </p>
        <Row>
            <Col md={6} className="p-3 mt-2">
            <ul>   
                <li className="p-1 ListItem"><span> Packing of Sofa</span></li>
                    <li className="p-1 ListItem">Packing of Sofa</li>
                    <li className="p-1">Packing of Bed</li>
                    <li className="p-1">Packing of Table and chairs</li>
                    <li className="p-1">Packing of Cupboards</li>
                    <li className="p-1">Packing of Fridge</li>
                    <li className="p-1">Packing of wall paintings and Sceneries</li>
                    <li className="p-1">Packing of Glassware’s</li>
                    <li className="p-1">Packing of Air Conditioners</li>
                    <li className="p-1">Packing of Washing Machines</li>
                    <li className="p-1">Packing of all utensils</li>
                    <li className="p-1">Packing of files, lamps, etc.</li>
                    <li className="p-1">Packing of Tv</li>
                    <li className="p-1">Packing of Crockery</li>
                    <li className="p-1">Packing of Grocery</li>
                </ul>
            </Col>
            <Col  md={6} className="p-3 mt-2">
            <img as={Image}  src={HomeShifting1} height="400px" width="100%"   className=" HomeShiftingImage"/>
            </Col>
            <Col md={6} className="p-3 mt-2">
            <img as={Image}  src={HomeShifting2} height="400px"  width="100%" className=" HomeShiftingImage"/>
            </Col>
            <Col md={6} className="p-3 mt-2">
            <p>We are proud to shout out that we claim to be the best Movers and
                        Packers in Lahore. We have a large
                    team of dedicated labour; our transport system is so satisfying. We will pack your items in a very
                    safe way providing you with the top services without any damage to your furniture and all items of
                    your home.</p>
                <p>We will make you stress-free by showing our services. We label items accordingly, so it will be
                    accessible when setting them in a new house.</p>
                <p>Apart from this, we will also be available to help you with resetting the furniture in a new place as
                    you want we will provide all kinds of help.

                </p>
                <p>Nowadays, it is hard to find the best home moving service at a cheap rate. Shifting locally is a very
                    stressful experience for companies and families. We will provide you with hassle-free services
                    without giving you any stress.

                </p>
            </Col>
        </Row>
        <Row>
        <h3 class="HomeShiftingHeading1">What You Get From Our Home Shifting Company</h3>
        <div class="fadeOutText">
            <Row>
                <Col md={3}>
                    <h4 class="HomeShiftingHeading2">CHEAP PRICE</h4>
                    <p>We always give you cheap rates then others companies</p>
                </Col>
                <Col md={3}>
                    <h4  class="HomeShiftingHeading2">FULLY SAFE & SECURE </h4>
                    <p>Our first priority is your goods safety</p>
                    </Col>
                <Col md={3}>
                    <h4  class="HomeShiftingHeading2">FURNITURE HANDLING</h4>
                    <p>We have an expert team that will easily maintain everything</p>
                    </Col>
                <Col md={3}>
                    <h4 class="HomeShiftingHeading2">PACKING IN BOXES</h4>
                    <p>All goods were packed in boxes and cotton</p>
                    </Col>
                    </Row>
          </div>


            </Row></Container>
            <Container>
       
            <Row>
                <Col md={6} className="mt-5">
                    <h5 className="HomeShiftingHeading">Home Shifting Services in Lahore</h5>
                    <p className="HomeShiftingText">We are offering a complete package at cheap rates to our clients. Suppose you are looking for the
                        most satisfying services near me in Lahore. We are ready to give you excellent services of
                        packing and unpacking, loading and unloading and reset all the items. After getting services
                        from us, we guarantee you that you will recommend us to your friends and relatives. Packing and
                        moving were not accessible before. People faced a lot of difficulties in moving home from one
                        place to another; they also met a lot of losses in moving. We pack items according to their
                        weight and size. We have double-layered boxes in which we put packed items. We get in contact
                        with our members for every update.</p>
                    <p className="HomeShiftingText">After packing and moving items, there is another mess to reset all the things, but it will also
                        not on you after contacting us. You will have to give orders, and our team will reassemble all
                        the furniture as you want. Pick your phone and place your order now.</p>
                    <h5 className=" HomeShiftingHeading">Home Shifting Services in Karachi
                    </h5>
                    <p className="HomeShiftingText">So, if you are looking for excellent packing and moving services near me, we are here and ready
                        to provide you the best services. Shifting may be an exciting and anxious scenario for people.
                        It is necessary to look for the best services before moving to a new location because of a
                        single damage matters. We provide the best services so, don’t feel hesitation, Feel free to
                        contact us and book your orders. We will give you all kinds of details that you want to know and
                        will satisfy you.</p>
                </Col>
              
                <Col  md={6} className=" mt-5">
                    <h5 className=" HomeShiftingHeading">Home Shifting Services in Islamabad</h5>
                    <p className="HomeShiftingText">Whenever you need us, we are just a phone call away from you. We are also providing the best
                        movers and packers Islamabad. Not everything is required to be packed safely and shifting to a
                        new location, but it also needs to be unpacked and then reset, and we are also providing these
                        services. Bulky items are hard to handle, but our professionally trained team members are
                        experts at handling heavy things they know how to pack it and how to shift it, so don’t worry we
                        will do our job as you want and according to your expectations.</p>
                    <p className="HomeShiftingText">We genuinely care about your expectations. We have a very high customer retention rate and our
                        customers come back to us for more services you can also contact us and ask for more details.
                        Our job is to make you satisfy by showing our services. We are making effortless Home Shifting
                        in Islamabad. So what is stopping you from calling us? Call us right now; we will never let you
                        down.</p>
                    <h5 className=" HomeShiftingHeading">Home Shifting Services in Rawalpindi
                    </h5>
                    <p className="HomeShiftingText">We are also available in Rawalpindi to provide near me services. During Shifting, People Usually
                        have worries about the damage; that’s why we are providing you the most satisfying and
                        well-planned services. After getting an order, now it is all up to us, you need to chill out and
                        have tea. Our rates are not that high which people can’t afford usually. We care about each
                        emotion of our clients and transfer safely according to your desire place. We are at the top
                        engine of google in packing and moving services.</p>
                </Col>
            </Row>
            <h5 className="text-center">Alhamdulilah,  we are providing services with our whole heart! In Shaa Allah, we will never let you down.</h5>
            <h3 className="text-center" class="HomeShiftingHeading1">How Our Home Shifting Services Works</h3>
            <Row  className="mt-5">
                <Col md={3} >
                    <img as={Image}  src={HomeShifting3}  alt=""  className="mb-5 HomeShiftingImage1"/>
                    <h5 class="HomeShiftingHeading2">01. Book Our Service</h5>
                    <p>First, you need to book services for more get a free a quote </p>

                </Col>
                <Col md={3}>
                    <img as={Image}  src={HomeShifting4}  alt="" className="mb-5 HomeShiftingImage1" />
                    <h5 class="HomeShiftingHeading2">02. We Pack Your luggage </h5>
                    <p>After booking a services our team will came to your home and pack your luggage</p>
                </Col>
                <Col md={3}>
                    <img as={Image}  src={HomeShifting5}  alt="" className="mb-5 HomeShiftingImage1 " />
                    <h5 class="HomeShiftingHeading2">03. We Move Your Goods 

                    </h5>
                    <p>When our goods were packed after we will move you goods to said location</p>
                </Col>
                <Col md={3}>
                    <img as={Image}  src={HomeShifting6}  alt="" className="mb-5 HomeShiftingImage1"/>
                    <h5 class="HomeShiftingHeading2" >04. We Deliver Safely

                    </h5>
                    <p>On the said location our team will unload your goods and rearrange safely</p>
                </Col>
            </Row>
            </Container>
            <footer className="Footer1">
       <Container >
 <Row className="Footer">
   <Col md={3} ><Image src="https://ishfaqmovers.com/wp-content/uploads/2018/10/mo-white.png"  ></Image>
<h6 className="HomeFooterText">ISHFAQ MOVERS has been one of the leading packers &
   Movers in Karachi-Pakistan for more than 10 years. We deliver a broad range of targeted national and 
   international moving services for home, 
  house, villa, apartments, offices as well as relocation solutions for every purpose.</h6>
   </Col>
   <Col md={3} className="mt-3">
     <h3>QUICK LINKS</h3>

     <p><FooterLinks/></p>
 
     <Route path="/about"><FooterLinks/></Route>
     <Route path="/contact"><FooterLinks/></Route>
   </Col>
   <Col md={3}  className="mt-3">
   <h3>OUR SERVICES</h3>
   <p><FooterLinks/></p>
 
 <Route path="/about"><FooterLinks/></Route>
 <Route path="/contact"><FooterLinks/></Route>
   </Col>
 </Row>
       </Container>
       <hr/>

       </footer>
        </div>
    )
}
export default HomeShifting;