import {Container , Button ,Card ,Row,Col , Image} from 'react-bootstrap'
import {TextField, FormControl , TextareaAutosize} from '@material-ui/core'
import SimpleSelect from '../HomeInquire '
import Cargo1 from '../asset/image/Cargo1.png'
import Cargo2 from '../asset/image/Cargo2.png'
import Cargo3 from '../asset/image/Cargo3.png'
import Cargo4 from '../asset/image/Cargo4.png'
import Cargo5 from '../asset/image/Cargo5.png'
import Cargo6 from '../asset/image/Cargo6.png'
import LocalShippingOutlinedIcon from '@material-ui/icons/LocalShippingOutlined';
import './CargoService.css'
import FooterLinks from '../FooterLink';
import FacebookIcon from '@material-ui/icons/Facebook';
import TwitterIcon from '@material-ui/icons/Twitter';
import InstagramIcon from '@material-ui/icons/Instagram';
import { Route, Router } from 'react-router-dom';

function Cargo() {
    function Qoute(){
        alert("SORRY :  This Service Is Unavailable At This Time")
        }
        function  NewsLetters() {
            alert("SORRY :  This Service Is Unavailable At This Time")
          }
    return (
        <div  style={{backgroundColor:"	 #eee2e2"}}>
        <Container  >
           <Row className="OfficeMovingMain">
               <Col md={6} className="mt-5">
               <h1 className="OfficeMovingHeading">Rapid Movers and Packers Cargo Services</h1>
               <ul>
                   <li>Rapid Movers and Packers Pakistan largest and trusted cargo services company,</li>
                   <li>Satisfying hundreds of customers daily</li>
                   <li>Choose a full range of cargo services over the country.</li>
                   <li>24/7 Customer Support</li>
                 
               </ul>
               <h3 className="OfficeMovingHeading">  +92-3112755732</h3>
               </Col>
               <Col md={6} className="mt-5">
               <Row>
                           <Col md={5}><TextField id="standard-basic" label="First Name" type="text" /></Col>
                           <Col md={5}><TextField id="standard-basic" label="Last Name" className="mb-3" /></Col>
                         
                           <Col md={5}><TextField id="standard-basic" label="Email" type="mail" /></Col>
                           <Col md={5}><TextField id="standard-basic" label="Phone Number" type="text"className="mb-3" /></Col>
                           
                           <Col md={5}><TextField id="standard-basic" label="Moving To"  /></Col>
                           <Col md={5}><TextField id="standard-basic" label="Moving From"  className="mb-3" /></Col>
                           <Col md={5} className="mb-3"><SimpleSelect /></Col>
                           </Row>
                           <Row>
                             <Col md={5}><Button type="submit"  onClick={()=>Qoute()}  className="OfficeMovingButton"  >Submit Form</Button></Col> <br/> <br/>
                             <Col md={5}><Button type="reset"  onClick={()=>Qoute()} className="OfficeMovingButton">Reset Form</Button></Col>
                           </Row>
               </Col>

           </Row> 
           <Row>
               <Col md={12} className="mt-4">
                   <h1 className="CargoServiceHeading">Rapid Movers Local Cargo Services</h1>
                   <p  className="CargoServiceText">If you are in need of general cargo services at an affordable price, Rapid Movers has precisely what you need. We offer some of the best and most careful services within a market competitive price range. The best part is that your Cargo will be in the hands of highly skilled and trained professionals, who take special care of the shipment and transportation process.</p>
                  <p className="CargoServiceText">At Rapid Movers, we understand the value of your goods, and we take pride in ensuring customer satisfaction when it comes to cargo transport. Cargo transport is a tricky job because it requires skills that the average movers might not have. However, our professional and experienced movers understand the science behind cargo shipping.</p> 
                  <p className="CargoServiceText">We are a cargo shipping company that makes sure our services meet high standards no matter what the conditions of the shipment are. The primary cargo containers we have are of impeccable quality, and we ensure that your valuable goods remain safe throughout the shipment. We expertly handle fragile goods and guarantee little to no breakage.</p>
                  <p className="CargoServiceText">Some of the related services that we provide when it comes to your cargo shipping include:</p>
                  <ul>
                      <li>Handling of clearance of goods through the customs agency,</li>
                      <li>Tracking of the cargo shipment,</li>
                      <li>Determining the best route for delivery,</li>
                      <li>Domestic cargo services</li>
                      <li>And much more!</li>
                  </ul>
                  <p className="CargoServiceText">Rapid Movers also provides these services within a reasonable cargo ship price! All of your cargo needs can be fulfilled at a competitive rate that you cannot find anyone else. The most convenient part is that Rapid Movers is available all over Pakistan!</p>
               </Col>
           </Row>
           <Row className="CargoService" >
               <Col md={12}>
                   <h1 >How To Get Our Services</h1>
                   <p className="CargoServiceText">Its is much easy to get a cargo service to move with Pakistan Largest company</p>
               </Col>
                         <Col md={4} className="mt-5" >
                         <img as={Image}  src={Cargo1}  className="CargoServices" />
                                 <h3 className="CargoServiceHeading1">Book A Services With Transparency</h3>
                                 <p className="CargoServiceText1">Book A Services to easily to mover your goods with transparency </p>
                             </Col>
                             <Col md={4} className="mt-5" >
                             <img as={Image}  src={Cargo2}  className="CargoServices" />
                                 <h3 className="CargoServiceHeading1">Reliable Cargo Service</h3>
                                 <p className="CargoServiceText1">We are providing door to door cargo service to facilitate our customers</p>
                             </Col>
                             <Col md={4} className="mt-5" >
                             <img as={Image}  src={Cargo3}  className="CargoServices" />
                                 <h3 className="CargoServiceHeading1">Pay Securely Before & After</h3>
                                 <p className="CargoServiceText1">We have a good  payment system online and offline you can pay easily from home</p>
                             </Col>
                      
                         </Row>
                         <Row>
                             <Col md={12}>
                                 <h1>How We are Cost Effective From Other Cargo Companies</h1>
                                 <p className="CargoServiceText">Our cost-effective and non-expedited cargo shipment services are unlike any others in Pakistan. Our team is entirely cooperative 
                                     and ensures that your Cargo is safely transported to you in our secure cargo container.</p>
                                     <p className="CargoServiceText">You can contact us today and get an accurate quote for your job! Our clients are always satisfied with the prices we offer, and many of them mention that Rapid Movers provides services in reasonable and highly affordable prices in comparison to other companies in the moving business.</p>
                                     <p className="CargoServiceText">No matter how near or far your Cargo has to go, we handle every client’s Cargo with the same care, and a consistent amount of effort is put into the shifting process. Our warehouse services will let you feel less stressed, while also being in control every step of the way with our tracking and tracing system.</p>
                                     <p className="CargoServiceText">At the final stage of the shipment, the Rapid Movers team provides our clients with a comprehensive report of what has been shipped and how any obstacles encountered were resolved. Rapid Movers offers the best cargo carrier services in the country, and you will be amazed by how efficient and punctual we are with these services</p>
                                     <p className="CargoServiceText">Rapid Movers understand the expectations that clients have from moving companies they hire, and we try out utmost to exceed those expectations. Our team takes pride in always putting the customer first and making the most of the time they spend a while on duty. Handling someone else’s possessions, after all, requires reliability, accuracy, and trustworthiness. Any job done by Rapid Movers is one done with productivity, care, honesty, and strength.</p>
                             </Col>
                         </Row>
                         <Row>
                             <Col md={4}>
                             <img as={Image}  src={Cargo4}      className="CargoServicesIcon"  />
                             <h4 className="CargoServicesIconHeading">WE MAKE IT FASTER</h4>
                             <p className="CargoServicesIconText">Not only by name we truly make it faster <br/> and easy for you</p>
                             </Col>
                             <Col md={4}>
                             <img as={Image}  src={Cargo5}      className="CargoServicesIcon"  />
                             <h4 className="CargoServicesIconHeading">SAVE AND SECURE MOVE</h4>
                             <p className="CargoServicesIconText">We always careful about our  customers <br/> and  their goods</p>

                             </Col>
                             <Col md={4}>
                             <img as={Image}  src={Cargo6}      className="CargoServicesIcon"  />
                             <h4 className="CargoServicesIconHeading" >DELEVERY ON TIME</h4>
                             <p className="CargoServicesIconText" >We have a professional team that makes it more faster, more secure to delivers on time</p>
                             </Col>
                             <h5 className="text-center">Alhamdulilah,  we are providing services with our whole heart! In Shaa Allah, we will never let you down.</h5>
                         </Row>
           </Container>
           <footer className="Footer1">
       <Container >
 <Row className="Footer">
   <Col md={3} ><Image src="https://ishfaqmovers.com/wp-content/uploads/2018/10/mo-white.png"  ></Image>
<h6 className="HomeFooterText">ISHFAQ MOVERS has been one of the leading packers &
   Movers in Karachi-Pakistan for more than 10 years. We deliver a broad range of targeted national and 
   international moving services for home, 
  house, villa, apartments, offices as well as relocation solutions for every purpose.</h6>
   </Col>
   <Col md={3} className="mt-3">
     <h3>QUICK LINKS</h3>

     <p><FooterLinks/></p>
 
     <Route path="/about"><FooterLinks/></Route>
     <Route path="/contact"><FooterLinks/></Route>
   </Col>
   <Col md={3}  className="mt-3">
   <h3>OUR SERVICES</h3>
   <p><FooterLinks/></p>
 
 <Route path="/about"><FooterLinks/></Route>
 <Route path="/contact"><FooterLinks/></Route>
   </Col>
   <Col md={3}  className="mt-">
   <h3>NEWSLETTER SIGNUP</h3>
  <h6>No spam, unsubscribe at any time.</h6>
   <TextField id="standard-basic" label="Email" type="mail"  /><br/>
   <Button onClick={()=>NewsLetters()} className="HomeButton mt-2 ">Subscribe</Button>
   <br/>
   <h5   className="mt-2 ">Contact On Social Media</h5>
   <a href="#"><FacebookIcon></FacebookIcon></a>
   <a href="#"><TwitterIcon></TwitterIcon></a>
   <a href="#"><InstagramIcon></InstagramIcon></a>
 
   </Col>
 </Row>
       </Container>
       <hr/>

       </footer>
              
           </div>
    )
    
}
export default Cargo;