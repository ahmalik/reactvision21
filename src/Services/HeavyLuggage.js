import {Container , Button ,Card ,Row,Col , Image} from 'react-bootstrap'
import {TextField, FormControl , TextareaAutosize} from '@material-ui/core'
import Luggage from '../asset/image/Luggage.png'
import Luggage2 from '../asset/image/luggage2.png'
import Luggage1 from '../asset/image/Luggage1.png'
import Luggage4 from '../asset/image/Luggage4.jpg'
import SimpleSelect from '../HomeInquire '
import { Route, Router } from 'react-router-dom';
import FooterLinks from '../FooterLink';
import FacebookIcon from '@material-ui/icons/Facebook';
import TwitterIcon from '@material-ui/icons/Twitter';
import InstagramIcon from '@material-ui/icons/Instagram';
import './HeavyLuggage.css'

function HeavyLuggage() {
    function Qoute(){
        alert("SORRY :  This Service Is Unavailable At This Time")
        }
        function  NewsLetters() {
            alert("SORRY :  This Service Is Unavailable At This Time")
          }
    return(
        <div style={{ backgroundColor: "	 #eee2e2" }}>
        <Container>
          <Row className="OfficeMovingMain">
            <Col md={6} className="mt-5">
              <h1 className="OfficeMovingHeading">
                Make your Day Stressless With Rapid Movers Office Moving Services
              </h1>
              <ul>
                <li>Day and Night Shifting Service</li>
                <li>Professional Team</li>
                <li>24/7 Customer Support</li>
              </ul>
              <h3 className="OfficeMovingHeading"> +92-3112755732</h3>
            </Col>
            <Col md={6} className="mt-5">
              <Row>
                <Col md={5}>
                  <TextField id="standard-basic" label="First Name" type="text" />
                </Col>
                <Col md={5}>
                  <TextField id="standard-basic" label="Last Name" className="mb-3" />
                </Col>
      
                <Col md={5}>
                  <TextField id="standard-basic" label="Email" type="mail" />
                </Col>
                <Col md={5}>
                  <TextField
                    id="standard-basic"
                    label="Phone Number"
                    type="text"
                    className="mb-3"
                  />
                </Col>
      
                <Col md={5}>
                  <TextField id="standard-basic" label="Moving To" />
                </Col>
                <Col md={5}>
                  <TextField
                    id="standard-basic"
                    label="Moving From"
                    className="mb-3"
                  />
                </Col>
                <Col md={5} className="mb-3">
                  <SimpleSelect />
                </Col>
              </Row>
              <Row>
                <Col md={5}>
                  <Button
                    type="submit"
                    onClick={() => Qoute()}
                    className="OfficeMovingButton"
                  >
                    Submit Form
                  </Button>
                </Col>{" "}
                <br /> <br />
                <Col md={5}>
                  <Button
                    type="reset"
                    onClick={() => Qoute()}
                    className="OfficeMovingButton"
                  >
                    Reset Form
                  </Button>
                </Col>
              </Row>
            </Col>
          </Row>
          <Row className="mt-5 ">
            <Col md={6}>
              <h1 className="LuggageMovingHeading">
                Reliable Door to Door Luggage Service
              </h1>
              <p className="LuggageMovingText">
                Rapid Movers and Packers provides stress-free door to door luggage
                services. The collection of all your belongings is not an easy
                process, especially when you are going abroad and you have an
                excessive amount of luggage with you. There is no need to collect to
                do all the home shifting on your own. When you book our services
                online, the company’s supervisor came to the customer’s house with
                professional luggage packers and movers.
              </p>
              <p className="LuggageMovingText">
                We provide every kind of luggage moving services at your door-step
                with full security. Every person is conscious about their home; we
                understand everything, and the customer’s safety and satisfaction is
                our priority. Customers just confirmed their order in only one step;
                message and send their guaranteed address.
              </p>
              <p className="LuggageMovingText">
                Furthermore, anyone is going abroad; they could not carry their heavy
                luggage and think any other person is doing that specific work. Movers
                company will provide you home luggage services at your door-step till
                the airport. We have years of excellence in the moving and packing
                field for belongings.
              </p>
            </Col>
            <Col md={6}>
              <img as={Image} src={Luggage} className="LuggageMovingImage" />
            </Col>
            <Col md={6} className="mt-3   ">
              <img as={Image} src={Luggage2} className="LuggageMovingImage" />
            </Col>
            <Col md={6} className="mt-3   ">
              <h1 className="LuggageMovingHeading">
                Abroad, Baggage Moving Services
              </h1>
              <p className="LuggageMovingText">
                Many times, persons going abroad will know that all airplanes
                organizations have strict rules for luggage services. Everything is
                dependent on the ticket level and limitations of a specific company.
                Rules and regulations are applied to every passenger ticket. They have
                limited place for items after this airplane management charges them
                per kilogram, and their prices are on the sky.
              </p>
              <p className="LuggageMovingText">
                Movers and Packers introduced luggage moving services in abroad, a
                brand concept in Pakistan. It reduces many costs as compared to
                charges of airlines. We packed you all luggage and moved them to your
                targeted location anywhere in abroad.
              </p>
              <h1 className="LuggageMovingHeading">Fast Luggage Moving Services</h1>
              <p className="LuggageMovingText">
                Speed is an essential factor for any company. Everyone knows that time
                is money nowadays. Movers and Packers have luggage moving services
                with an extra fast delivery system. Customers make us a call, and our
                management community arranges everything for yo
              </p>
              <p className="LuggageMovingText">
                Our moving experts had done many projects for luggage moving in
                different cities. Our main tasks are luggage packing services to the
                airport or going abroad by using shipping. We have fast, reliable
                shipping services at cheap rates. Customer’s budgets are always in our
                minds. That is the reason we have worldwide happy customers.
              </p>
            </Col>
            <Col md={6} className="mt-3   ">
              <h1 className="LuggageMovingHeading">Store Your Home Luggage</h1>
              <p className="LuggageMovingText">
                Suppose you need a warehouse for your luggage for the short term or
                long term services. Rapid Movers and Packers provide you the best
                storage services for all belongings or any luggage for the customized
                period.
              </p>
              <p className="LuggageMovingText">
                If you are going anywhere abroad and wanted to store your items, we
                will help in every kind of luggage shifting service. We have
                reasonable prices compared to other competitors. Competitors have
                inadequate services and charge a high fee for storing items. For more
                information, visit our website or call us. Either search on google
                “luggage movers near me”.
              </p>
            </Col>
            <Col md={6} className="mt-3">
              <img as={Image} src={Luggage1} className="LuggageMovingImage" />
            </Col>
            <Col md={6} className="mt-5">
              <img as={Image} src={Luggage4} className="LuggageMovingImage" />
            </Col>
            <Col md={6} className="mt-3">
              <h1 className="LuggageMovingHeading">Safe Way to Travel</h1>
              <p className="LuggageMovingText">
                Shipping is the Best Way of traveling for going anywhere in the world.
                Rapid Movers and Packers have excellent home luggage shifting services
                outside the country. Shipping is the Fastest Way to deliver any items
                of luggage and baggage at reasonable rates. We shall not face any
                trouble with crowded areas and meaning fewer charges.
              </p>
              <p className="LuggageMovingText">
                Luggage moving will become accessible by shipping services. Save time
                and avoid useless services from different companies when customers
                search for the best luggage moving companies in Pakistan, mostly seen
                that movers and packers are in the top position.
              </p>
              <h1 className="LuggageMovingHeading">
                Contract your Home Luggage Shipping with the Verified Company
              </h1>
              <p className="LuggageMovingText">
                We understand that all items are precious and valuable, and you should
                not lose them at any price. Movers and Packers Lahore have a
                professional team for your home luggage and any luggage. We are a
                trustworthy company and have appropriately licensed global shipping.
              </p>
              <p className="LuggageMovingText">
                As I said earlier, shipping is the best process of sending your
                luggage globally with reasonable-rates. Did you want to professional
                luggage movers? Go to our website of Rapid Movers and Packers, where
                you can find every moving service worldwide.
              </p>
            </Col>
            <Col md={12}>
              <h1 className="LuggageMovingHeading">Luggage Moving Services</h1>
              <p className="LuggageMovingText">
                Are you want to go to foreign countries and you have an excessive
                number of items in baggage? No problem. We provide all foreign baggage
                moving services. Our professional team pack your luggage, load to the
                container, and move them in the ship. We have a good relation with
                shipping companies.
              </p>
              <p className="LuggageMovingText">
                We have cheap-rates for going anywhere in the world as well as we have
                happy customers internationally.
              </p>
            </Col>
            <h5 className="text-center">
              Alhamdulilah, we are providing services with our whole heart! In Shaa
              Allah, we will never let you down.
            </h5>
          </Row>
        </Container>
      
        <footer className="Footer1">
          <Container>
            <Row className="Footer">
              <Col md={3}>
                <Image src="https://ishfaqmovers.com/wp-content/uploads/2018/10/mo-white.png"></Image>
                <h6 className="HomeFooterText">
                  ISHFAQ MOVERS has been one of the leading packers & Movers in
                  Karachi-Pakistan for more than 10 years. We deliver a broad range of
                  targeted national and international moving services for home, house,
                  villa, apartments, offices as well as relocation solutions for every
                  purpose.
                </h6>
              </Col>
              <Col md={3} className="mt-3">
                <h3>QUICK LINKS</h3>
      
                <p>
                  <FooterLinks />
                </p>
      
                <Route path="/about">
                  <FooterLinks />
                </Route>
                <Route path="/contact">
                  <FooterLinks />
                </Route>
              </Col>
              <Col md={3} className="mt-3">
                <h3>OUR SERVICES</h3>
                <p>
                  <FooterLinks />
                </p>
      
                <Route path="/about">
                  <FooterLinks />
                </Route>
                <Route path="/contact">
                  <FooterLinks />
                </Route>
              </Col>
              <Col md={3} className="mt-">
                <h3>NEWSLETTER SIGNUP</h3>
                <h6>No spam, unsubscribe at any time.</h6>
                <TextField id="standard-basic" label="Email" type="mail" />
                <br />
                <Button onClick={() => NewsLetters()} className="HomeButton mt-2 ">
                  Subscribe
                </Button>
                <br />
                <h5 className="mt-2 ">Contact On Social Media</h5>
                <a href="#">
                  <FacebookIcon></FacebookIcon>
                </a>
                <a href="#">
                  <TwitterIcon></TwitterIcon>
                </a>
                <a href="#">
                  <InstagramIcon></InstagramIcon>
                </a>
              </Col>
            </Row>
          </Container>
          <hr />
        </footer>
      </div>
      
    )
}
export default HeavyLuggage;