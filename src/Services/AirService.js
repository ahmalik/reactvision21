import {Container , Button ,Card ,Row,Col , Image} from 'react-bootstrap'
import {TextField, FormControl , TextareaAutosize} from '@material-ui/core'
import SimpleSelect from '../HomeInquire '
import AirCargo from '../asset/image/AirCargo.png'
import AirCargo1 from '../asset/image/AirCargo1.png'
import AirCargo2 from '../asset/image/AirCargo2.png'
import { Route, Router } from 'react-router-dom';
import FooterLinks from '../FooterLink';
import FacebookIcon from '@material-ui/icons/Facebook';
import TwitterIcon from '@material-ui/icons/Twitter';
import InstagramIcon from '@material-ui/icons/Instagram';
import './AirCargo.css'

function AirService() {
    function Qoute(){
        alert("SORRY :  This Service Is Unavailable At This Time")
        }
        function  NewsLetters() {
            alert("SORRY :  This Service Is Unavailable At This Time")
          }
    return(
        <div style={{ backgroundColor: "	 #eee2e2" }}>
        <Container>
          <Row className="OfficeMovingMain">
            <Col md={6} className="mt-5">
              <h1 className="OfficeMovingHeading">International Air Freight Services</h1>
              <ul>
                <li>Day and Night Service</li>
                <li>Professional Team</li>
                <li>24/7 Customer Support</li>
              </ul>
              <h3 className="OfficeMovingHeading"> +92-3112755732</h3>
            </Col>
            <Col md={6} className="mt-5">
            <form>
              <Row>
              
                <Col md={5}>
                  <TextField id="standard-basic" label="First Name" type="text" />
                </Col>
                <Col md={5}>
                  <TextField id="standard-basic" label="Last Name" className="mb-3" />
                </Col>
      
                <Col md={5}>
                  <TextField id="standard-basic" label="Email" type="mail" />
                </Col>
                <Col md={5}>
                  <TextField
                    id="standard-basic"
                    label="Phone Number"
                    type="text"
                    className="mb-3"
                  />
                </Col>
      
                <Col md={5}>
                  <TextField id="standard-basic" label="City" />
                </Col>
                <Col md={5}>
                  <TextField id="standard-basic" label="Country" className="mb-3" />
                </Col>
                <Col md={5} className="mb-3">
                  <SimpleSelect />
                </Col>
              </Row>
              <Row>
                <Col md={5}>
                  <Button
                    type="submit"
                    onClick={() => Qoute()}
                    className="OfficeMovingButton"
                  >
                    Submit Form
                  </Button>
                </Col>{" "}
                <br /> <br />
                <Col md={5}>
                  <Button
                    type="reset"

                    className="OfficeMovingButton"
                  >
                    Reset Form
                  </Button>
                </Col>
              </Row> </form>
            </Col>
          </Row> 
          <Row>
              <Col md={12}>
                  <h1 className="AirCargoHeading">International Air Freight Services</h1>
                  <p className="AirCargoText">Airfreight means that moving bulky items to another city via plane. Rapid Movers and Packers are the top reliable and trustworthy international air freight services located in many Pakistan cities. The primary element of our Logistics Management System is Air Freight Cargo Services. We can get ahead your airfreight shipments to your favourite destinations – all around the international, at any time, huge or small. Still, continuously with the same high standard provider, you expect of us.</p>
                  <p className="AirCargoText">We are one of the quickest transport alternatives for air shipping items & cargo worldwide. Book your deliveries from our best services and get a personalized air freight cargo quote for delivery. </p>
                  <p className="AirCargoText">International freight Services Company located in Lahore providing best air Cargo and logistics services at your doorstep. We can ship the cargo by air from Pakistani airports as your required destination. Our air freight rates per kg are different as compared to other international air shipping companies.</p>

              </Col>
              <Col md={6}>
                  <h1 className="AirCargoHeading">Transporting your Goods via Air Freight Services</h1>
                  <p className="AirCargoText" >Air Cargo Shipping is highly beneficial for import and export business. It is advantageous if you send or receive bulky items from one country to another with reasonable rates. Rapid Movers is one of the best air shipping services Provider Company delivers your goods from one city or country worldwide with proper documentation in cheap rates.</p>
                  <p className="AirCargoText" >We are the best in the domain of air freight services at reasonable rates. We also provide complete logistics services and is the best logistics services Provider Company in Pakistan. We have created good relations with air cargo airlines and top air—that why we deliver customers belongings at their doorstep within 24 hours.</p>

              </Col>
              <Col md={6}>
              <img as={Image} src={AirCargo} className="AirCargoImage" />
            </Col>
            <Col md={6}>
              <img as={Image} src={AirCargo1} className="AirCargoImage"  height="650px  " />
            </Col>
            <Col md={6}>
                <h1 className="AirCargoHeading">Goods Safety & Arrival Timings </h1>
                <p  className="AirCargoText">The pleasant part about air freight services is that you can track your goods and our plan arrive on time at your required airport. It is our top priority to deliver your items quickly according to your demand. We have delivered many projects during the day, and many plans are running during the day & night time routine. </p>
                <p  className="AirCargoText">Our international air cargo services have met the people’s requirements and always delivered customers packages on time with pure safety. 

</p>
<p  className="AirCargoText">We pack your items, goods, luggage & other useful items in the best way with proper air freight services documentation. Send to other your loveliest country within your estimated time and date. Your goods and packages remain in the best condition.</p>
<p  className="AirCargoText">Everyone knows that airport security is much better than that of sea cargo services. In this way, the percentage of break items and stolen items reduces very quickly. Safety of customer’s items is our priority.
    </p>
    <h1 className="AirCargoHeading">Faster Goods Moving Experience with Air Cargo</h1>
    <p className="AirCargoText">We provide the fastest goods moving experience with air cargo services, and we have a matchless good delivery experience, unlike other air transported companies. Airfreight services are more reliable and safer as compared to other sea & land fascinated services</p>
    <p className="AirCargoText"> Our air freight services are not only reliable but also cost-effective in many different patterns.</p>
            </Col>
            <Col md={6}>
                <h1 className="AirCargoHeading">Why Choose International Air Freight Forwarding Services?</h1>
                <p className="AirCargoText">Rapid Movers offers international air cargo services in various cities of Pakistan which include Karachi, Lahore, Islamabad, Multan, Faisalabad and many more.</p>
                <p className="AirCargoText">1: We have a collaboration with the best air freight companies globally.
Rapid movers provide you more than just one mode of air freight transportation.</p>
<p className="AirCargoText">2: We have references to help execute every process efficiently.
</p>
<p className="AirCargoText">3: Rapid movers have around two decades in the field of freight forwarding internationally.</p>
        <p className="AirCargoText">4: We provide you with cost-effective rates according to your budget.</p>
        <p className="AirCargoText">5: We guarantee to deliver to you what we promise.</p>
      
            </Col>
            <Col md={6}>
              <img as={Image} src={AirCargo2} className="AirCargoImage"  />
            </Col>
          </Row>
          </Container>
          <footer className="Footer1">
          <Container>
            <Row className="Footer">
              <Col md={3}>
                <Image src="https://ishfaqmovers.com/wp-content/uploads/2018/10/mo-white.png"></Image>
                <h6 className="HomeFooterText">
                  ISHFAQ MOVERS has been one of the leading packers & Movers in
                  Karachi-Pakistan for more than 10 years. We deliver a broad range of
                  targeted national and international moving services for home, house,
                  villa, apartments, offices as well as relocation solutions for every
                  purpose.
                </h6>
              </Col>
              <Col md={3} className="mt-3">
                <h3>QUICK LINKS</h3>
      
                <p>
                  <FooterLinks />
                </p>
      
                <Route path="/about">
                  <FooterLinks />
                </Route>
                <Route path="/contact">
                  <FooterLinks />
                </Route>
              </Col>
              <Col md={3} className="mt-3">
                <h3>OUR SERVICES</h3>
                <p>
                  <FooterLinks />
                </p>
      
                <Route path="/about">
                  <FooterLinks />
                </Route>
                <Route path="/contact">
                  <FooterLinks />
                </Route>
              </Col>
              <Col md={3} className="mt-">
                <h3>NEWSLETTER SIGNUP</h3>
                <h6>No spam, unsubscribe at any time.</h6>
                <TextField id="standard-basic" label="Email" type="mail" />
                <br />
                <Button onClick={() => NewsLetters()} className="HomeButton mt-2 ">
                  Subscribe
                </Button>
                <br />
                <h5 className="mt-2 ">Contact On Social Media</h5>
                <a href="#">
                  <FacebookIcon></FacebookIcon>
                </a>
                <a href="#">
                  <TwitterIcon></TwitterIcon>
                </a>
                <a href="#">
                  <InstagramIcon></InstagramIcon>
                </a>
              </Col>
            </Row>
          </Container>
          <hr />
        </footer>
          </div>
    )
    
}
export default AirService;