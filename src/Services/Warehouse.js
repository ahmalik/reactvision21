import {Container , Button ,Card ,Row,Col , Image} from 'react-bootstrap'
import {TextField, FormControl , TextareaAutosize} from '@material-ui/core'
import SimpleSelect from '../HomeInquire '
import WarehouseStorage from '../asset/image/warehouse-storage.png'


import FooterLinks from '../FooterLink';
import FacebookIcon from '@material-ui/icons/Facebook';
import TwitterIcon from '@material-ui/icons/Twitter';
import InstagramIcon from '@material-ui/icons/Instagram';
import { Route, Router } from 'react-router-dom';
import '../Services/Warehouse.css'

function Warehouse() {
    function Qoute(){
        alert("SORRY :  This Service Is Unavailable At This Time")
        }
        function  NewsLetters() {
            alert("SORRY :  This Service Is Unavailable At This Time")
          }
    return(
        <div  style={{backgroundColor:"	 #eee2e2"}}>
             <Container  >
                <Row className="OfficeMovingMain">
                    <Col md={6} className="mt-5">
                    <h1 className="OfficeMovingHeading">PAKISTAN'S NO.1 GOODS TRANSPORT SERVICES.</h1>
                    <ul>
                        <li>Full Container Load (FCL).</li>
                        <li>Contract Carriage.</li>
                        <li>Loading & Offloading.</li>
                        <li>24/7 Customer Support</li>
                      
                    </ul>
                    <h3 className="OfficeMovingHeading">  +92-3112755732</h3>
                    </Col>
                    <Col md={6} className="mt-5">
                    <Row>
                                <Col md={5}><TextField id="standard-basic" label="First Name" type="text" /></Col>
                                <Col md={5}><TextField id="standard-basic" label="Last Name" className="mb-3" /></Col>
                              
                                <Col md={5}><TextField id="standard-basic" label="Email" type="mail" /></Col>
                                <Col md={5}><TextField id="standard-basic" label="Phone Number" type="text"className="mb-3" /></Col>
                                
                                <Col md={5}><TextField id="standard-basic" label="Moving To"  /></Col>
                                <Col md={5}><TextField id="standard-basic" label="Moving From"  className="mb-3" /></Col>
                                <Col md={5} className="mb-3"><SimpleSelect /></Col>
                                </Row>
                                <Row>
                                  <Col md={5}><Button type="submit"  onClick={()=>Qoute()}  className="OfficeMovingButton"  >Submit Form</Button></Col> <br/> <br/>
                                  <Col md={5}><Button type="reset"  onClick={()=>Qoute()} className="OfficeMovingButton">Reset Form</Button></Col>
                                </Row>
                    </Col>

                </Row> 
                <Row><Col md={12}>
                    <h1 className="WarehouseServiceHeading mt-4">Warehouse and Storage Services In Pakistan</h1>
                </Col>
                <Col md={6}>
                    <p>For anyone who has a lot of possessions, but not enough storage space, the solution to such a problem is warehouse storage. Warehousing is a fancy term for getting extra space to put your goods in, mainly if you have run out of room in your home. 
                        Many people in Pakistan require additional storage space as a means to get more organized with what they own.</p>
                        <p>One of the best warehousing companies in Pakistan is Rapid Movers. They have an extensive storage unit that they can also help you organize to meet your customized needs. Each client is given a separate team, and the unit’s size depends on your
                 own needs and what you plan to store. If you want, you can also get a shared warehouse space and leave the logistics to us.</p>
                 <p>Some of the warehousing solutions that you can avail with Rapid Movers include:</p>
                 <ul>
                     <li>Storing your Cargo in a warehouse,</li>
                     <li>Warehouse shipping,</li>
                     <li>Short term warehousing,</li>
                     <li>Updates about your warehouse unit</li>
                     <li>Logistical services</li>
                     <li>Security for the storage unit</li>
                     <li>And many more!</li>
                 </ul>
                 <p>If you search for “the best public warehouse near me,” one of the first results to turn up will be Rapid Movers. Rapid Movers will also probably be the top result if you search for “number 1 logistics warehouse near me”. Our five-star services keep your valuables safe, secure, and on-record. 
                     We provide warehousing and distribution services that are unmatched by any other company in the country.</p>
                     <p>One of the biggest problems people face when searching for such services is finding secure warehousing. This is because many companies do not hire trusted employees, and the risk for threat when it comes to storage in a warehouse is enormous. However, Rapid Movers 
                         takes all of the needed measures to safeguard against any troubles. Our team is highly cooperative and extremely reliable.</p>
                <p>Shipping to a warehouse is also simple if you choose Rapid Movers. We will handle the entire process with ease, and this will make it convenient for you to oversee the warehousing process as a whole. Our storage 
                    and warehousing services are accurate, and we provide our clients with the support they need every step of the way.</p>
                </Col>
                <Col md={6}>
                <img as={Image}  src={WarehouseStorage}  className="WarehouseStorage" />

                <p className="mt-5" >We will give you all kinds of details that you want to know and will satisfy you.</p>
                <p>Warehouse storage units can often be challenging to find, especially for people living in remote locations. However, Rapid Movers has extensive locations and branches.
                     Even if you cannot find one nearby, we can help you find a storage unit with ease.</p>
                     <p>We take special care in placing your goods in an organized manner and under your supervision. Our team brings their time to understand where everything has been set, and a comprehensive list is then prepared to ensure nothing goes missing. 
                         Guards are stationed at the warehouse to ensure that only authorized individuals can access the warehouse.</p>
                         <p>We take special care in placing your goods in an organized manner and under your supervision. Our team brings their time to understand where everything has been set, and a comprehensive list is then prepared to ensure nothing goes missing. 
                         Guards are stationed at the warehouse to ensure that only authorized individuals can access the warehouse.</p>
                </Col>
                <h5 className="text-center">Alhamdulilah,  we are providing services with our whole heart! In Shaa Allah, we will never let you down.</h5>
                </Row>
                     </Container  >
                     <footer className="Footer1">
       <Container >
 <Row className="Footer">
   <Col md={3} ><Image src="https://ishfaqmovers.com/wp-content/uploads/2018/10/mo-white.png"  ></Image>
<h6 className="HomeFooterText">ISHFAQ MOVERS has been one of the leading packers &
   Movers in Karachi-Pakistan for more than 10 years. We deliver a broad range of targeted national and 
   international moving services for home, 
  house, villa, apartments, offices as well as relocation solutions for every purpose.</h6>
   </Col>
   <Col md={3} className="mt-3">
     <h3>QUICK LINKS</h3>

     <p><FooterLinks/></p>
 
     <Route path="/about"><FooterLinks/></Route>
     <Route path="/contact"><FooterLinks/></Route>
   </Col>
   <Col md={3}  className="mt-3">
   <h3>OUR SERVICES</h3>
   <p><FooterLinks/></p>
 
 <Route path="/about"><FooterLinks/></Route>
 <Route path="/contact"><FooterLinks/></Route>
   </Col>
   <Col md={3}  className="mt-">
   <h3>NEWSLETTER SIGNUP</h3>
  <h6>No spam, unsubscribe at any time.</h6>
   <TextField id="standard-basic" label="Email" type="mail"  /><br/>
   <Button onClick={()=>NewsLetters()} className="HomeButton mt-2 ">Subscribe</Button>
   <br/>
   <h5   className="mt-2 ">Contact On Social Media</h5>
   <a href="#"><FacebookIcon></FacebookIcon></a>
   <a href="#"><TwitterIcon></TwitterIcon></a>
   <a href="#"><InstagramIcon></InstagramIcon></a>
 
   </Col>
 </Row>
       </Container>
       <hr/>

       </footer>
        </div>
    )
}
export default Warehouse;