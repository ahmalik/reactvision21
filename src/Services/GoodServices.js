import {Container , Button ,Card ,Row,Col , Image} from 'react-bootstrap'
import {TextField, FormControl , TextareaAutosize} from '@material-ui/core'
import SimpleSelect from '../HomeInquire '
import GoodTransports from '../asset/image/goods-services.jpg'
import GoodTransports1 from '../asset/image/goods-services1.jpg'
import FooterLinks from '../FooterLink';
import FacebookIcon from '@material-ui/icons/Facebook';
import TwitterIcon from '@material-ui/icons/Twitter';
import InstagramIcon from '@material-ui/icons/Instagram';
import { Route, Router } from 'react-router-dom';
import './GoodServices.css'

function Goods(){
    function Qoute(){
        alert("SORRY :  This Service Is Unavailable At This Time")
        }
        function  NewsLetters() {
            alert("SORRY :  This Service Is Unavailable At This Time")
          }
    return(
        <div  style={{backgroundColor:"	 #eee2e2"}}>
  <Container  >
                <Row className="OfficeMovingMain">
                    <Col md={6} className="mt-5">
                    <h1 className="OfficeMovingHeading">PAKISTAN'S NO.1 GOODS TRANSPORT SERVICES.</h1>
                    <ul>
                        <li>Full Container Load (FCL).</li>
                        <li>Contract Carriage.</li>
                        <li>Loading & Offloading.</li>
                        <li>24/7 Customer Support</li>
                      
                    </ul>
                    <h3 className="OfficeMovingHeading">  +92-3112755732</h3>
                    </Col>
                    <Col md={6} className="mt-5">
                    <Row>
                                <Col md={5}><TextField id="standard-basic" label="First Name" type="text" /></Col>
                                <Col md={5}><TextField id="standard-basic" label="Last Name" className="mb-3" /></Col>
                              
                                <Col md={5}><TextField id="standard-basic" label="Email" type="mail" /></Col>
                                <Col md={5}><TextField id="standard-basic" label="Phone Number" type="text"className="mb-3" /></Col>
                                
                                <Col md={5}><TextField id="standard-basic" label="Moving To"  /></Col>
                                <Col md={5}><TextField id="standard-basic" label="Moving From"  className="mb-3" /></Col>
                                <Col md={5} className="mb-3"><SimpleSelect /></Col>
                                </Row>
                                <Row>
                                  <Col md={5}><Button type="submit"  onClick={()=>Qoute()}  className="OfficeMovingButton"  >Submit Form</Button></Col> <br/> <br/>
                                  <Col md={5}><Button type="reset"  onClick={()=>Qoute()} className="OfficeMovingButton">Reset Form</Button></Col>
                                </Row>
                    </Col>
                </Row>

                <h1  className="GoodTransportsHeading">Why Choose Rapid Movers?</h1>
                <p>Rapid Movers Goods Transport Company provides its services to major cities of Pakistan (Islamabad, Karachi, Lahore, Faisalabad, Multan, Rawalpindi and All over Pakistan) China, Turkey, USA and Worldwide Goods Transport Services by air, sea,
                     and also by road. We also offer our transportation service to all over Pakistan with door to door delivery. </p>
                     <p className="GoodTransportsText">We offer full-fledged export and import services and transportation of goods for popular all ports. Rapid Movers have a huge quantity of precious customers worldwide who trust upon us due to our quality work. Own 300+ (Including Containers, Flat Bed, Low Bed, Half Body Trailer) Vehicles with 
                         professional registered drivers dedicated To their Duties Which Reduces Your Chance Of Risk And Saves Your Money And Time.  </p>
                         <Row>
                             <Col md={6}>
                                 <h1 className="GoodTransportsHeading">Goods Transport Services</h1>
                                 <p className="GoodTransportsText">Rapid Movers is the leading goods Transport Company with best and reliable transportation systems. Our sophisticated transportation services played a decent 
                                     role in the Transportation Service Company during any problem about moving your heavy items from one place to another.</p>
                                     <p className="GoodTransportsText">We have years of experience in transportation service to fulfil the purchasers’ wants and needs by making specific safe methods. These services are gaining quality these days. We also build communication 
                                            with our targeted market. Rapid Movers goods Transport Company in Lahore offers transportation services at your doorstep.</p>
                                            <p>Our Company is registered in all Central Asia Transportation License and also from CPEC. Rapid Movers Goods Transport Company is growing its customers trust upon us, and we do not charge any hidden fee for your moving.</p>
                                            <p>Rapid Movers has all the solutions with a wide range of vehicles fully equipped for well-maintained transportation. Our drivers are fully experienced and motivated in customer’s belongings safety and maintain our fleet’s excellent standard. Our goods transport vehicles are regularly serviced by professionals and strictly supported for the protection of clients items.</p>
                             </Col>
                             <Col md={6}>
                             <img as={Image}  src={GoodTransports}  className="GoodTransports" />
                             </Col>
                          
                         </Row>
                         <Row className="GoodsTransport" >
                         <Col md={4} className="mt-5" >
                                 <h3 className="GoodTransportsHeading1">Fast Delivery Service</h3>
                                 <p className="GoodTransportsText1">Rapid movers offer all types of goods transport services with same-day delivery for adjoining state cities. We also provide same-day pick-up between you receive the highest level of convenience. There are many transportation companies, but they often seem to not care about the client’s 
                                     precious items, and their drivers have no experience. The results cause a loss of valuable items.</p>
                             </Col>
                             <Col md={4} className="mt-5" >
                                 <h3 className="GoodTransportsHeading1">Special Shipment</h3>
                                 <p className="GoodTransportsText1">At Rapid Movers, our clients are our top priority. Unlike other freight companies, we provide customized household transport services and guarantee careful handling and care at all stages. Our transportation trucks are fully equipped with the latest techniques to handle all kinds of cargo, and we provide
                                  the best shipment of hazardous materials in a careful and specialized environment.</p>
                             </Col>
                             <Col md={4} className="mt-5" >
                                 <h3 className="GoodTransportsHeading1">Trained Staff</h3>
                                 <p className="GoodTransportsText1">All our goods packing and moving staff is highly skilled, and specially trained for bulky transport services at your doorstep. We put a specific type of emphasis on hospitality and uniformed drivers with radio-dispatched equipment will be at your service. We have excellent behaviour with all our precious customers, and our all staff is also polite and 
                                 honest with work and also have good relationships with customers.</p>
                             </Col>
                      
                         </Row>
                         <Row>
                         <Col md={6} className="mt-5">
                        <img as={Image}  src={GoodTransports1}  className="GoodTransports" />
                        </Col>
                        <Col md={5} className="offset-md-1 mt-5 ">
                        <h3 className="GoodTransportsHeading">Goods Transport Company in Karachi</h3>
                        <p className="GoodTransportsText">We at Rapid Movers also provide goods transport services in Lahore for international stock transportation, imports and exports are in business. In the list of goods transport companies in Lahore,
                             our transportation company is outstanding, and we had served thousands of satisfied customers.</p>
                             <p className="GoodTransportsText">Heavy Transport operations are coordinated centrally and offer outstanding bulky items transportation services that include Heavy Transport, industries services, and Project Management. It is backed up by our Company’s investment in state of the art Hydraulic Trailer equipment and in-house engineering facilities to modify trailers according to the cargo’s nature and shape.</p>
                             <h3 className="GoodTransportsHeading"> Special Services & Urgent Delivery</h3>
                             <p className="GoodTransportsText">1: Do you not want delays for your goods transport? Call upon our particular weekend cargo tracking service, and our drivers are ready for any transportation.</p>
                             <p className="GoodTransportsText">2: You can also go for our select after-hours pick-up and early morning delivery service if you are going with a tight schedule.</p>
                             <p className="GoodTransportsText">3: Rapid Movers offer inside delivery, residential service, lift gate, deliveries for hotel & conventions, etc.</p>
                           
                             </Col>
                             <h5 className="text-center">Alhamdulilah,  we are providing services with our whole heart! In Shaa Allah, we will never let you down.</h5>
                         </Row>  </Container  >
                         <footer className="Footer1">
       <Container >
 <Row className="Footer">
   <Col md={3} ><Image src="https://ishfaqmovers.com/wp-content/uploads/2018/10/mo-white.png"  ></Image>
<h6 className="HomeFooterText">ISHFAQ MOVERS has been one of the leading packers &
   Movers in Karachi-Pakistan for more than 10 years. We deliver a broad range of targeted national and 
   international moving services for home, 
  house, villa, apartments, offices as well as relocation solutions for every purpose.</h6>
   </Col>
   <Col md={3} className="mt-3">
     <h3>QUICK LINKS</h3>

     <p><FooterLinks/></p>
 
     <Route path="/about"><FooterLinks/></Route>
     <Route path="/contact"><FooterLinks/></Route>
   </Col>
   <Col md={3}  className="mt-3">
   <h3>OUR SERVICES</h3>
   <p><FooterLinks/></p>
 
 <Route path="/about"><FooterLinks/></Route>
 <Route path="/contact"><FooterLinks/></Route>
   </Col>
   <Col md={3}  className="mt-">
   <h3>NEWSLETTER SIGNUP</h3>
  <h6>No spam, unsubscribe at any time.</h6>
   <TextField id="standard-basic" label="Email" type="mail"  /><br/>
   <Button onClick={()=>NewsLetters()} className="HomeButton mt-2 ">Subscribe</Button>
   <br/>
   <h5   className="mt-2 ">Contact On Social Media</h5>
   <a href="#"><FacebookIcon></FacebookIcon></a>
   <a href="#"><TwitterIcon></TwitterIcon></a>
   <a href="#"><InstagramIcon></InstagramIcon></a>
 
   </Col>
 </Row>
       </Container>
       <hr/>

       </footer>
              
        </div>
    )
}
export default Goods;