import './Card.css'
import {Container , Row,Col, Button , } from 'react-bootstrap'
import HomeShifting from './asset/image/Home Shifting.png';
import OfficeMoving from './asset/image/Office Moving.jpg'
import VehicleMovingService from './asset/image/Vehicle Moving Service.png'
import CargoService from './asset/image/Cargo Services.jpg'
import WareHouse from './asset/image/Warehouse and Storage Services.jpg'
import GoodsTransport from './asset/image/Good Transport.jpg'
import LuggageMoving from './asset/image/Luggage Moving.jpg'
import CustomClearance from './asset/image/Custom Clearance.jpeg'
import AirFreightService from './asset/image/Air Freight Services.jpg'
import React from 'react';
import FacebookIcon from '@material-ui/icons/Facebook';
import { Link } from 'react-router-dom';


function Cards(){

  return(
    <div>


    <Container>
    <Row >
      <Col md={4}  className="mb-3">
       <div class="cards">
    <div class="image">
      <img as={Image}  src={HomeShifting} height="300px" />
      <h3 class="bg-danger" className="CardText">Home Shifting</h3>
      
    </div>
    <div class="details"  width="auto">
      <div class="center"  width="auto">
        <h1>Home Shifting</h1>
        <p>Day & Night Shifting Service <br/>
          Professional Team <br/>
24/7 Customer Support</p>    
<Button className="ReadMore" variant="secondary"><Link to="/Services/HomeShifting"   className="LinkItem">Read More</Link> </Button>

       
      </div>
    </div>
  </div>
  </Col>
  <Col md={4}  className="mb-3">
       <div class="cards">
    <div class="image">
      <img as={Image}  src={OfficeMoving} height="300px" />
      <h3 class="bg-danger" className="CardText">Office Moving</h3>

    </div>
    <div class="details"  width="auto">
      <div class="center"  width="auto">
        <h1>Office Moving <br/>Service</h1>
        <p>24 Hours Shifting Service  Safe And Secure   <br/>Experienced team 24/7  <br/>Customer Support</p>
        <Button className="ReadMore" variant="secondary"><Link to="/Services/OfficeMoving"   className="LinkItem">Read More</Link></Button>
        
      </div>
    </div>
  </div>
  </Col>
  <Col md={4}  className="mb-3">
       <div class="cards">
    <div class="image">
      <img as={Image}  src={VehicleMovingService} height="300px" />
      <h3 class="bg-danger" className="CardText">VEHICLE MOVING</h3>

    </div>
    <div class="details"  width="auto">
      <div class="center"  width="auto">
        <h1>Vehicle Moving Service<br/></h1>
        <p>Safe And Secure <br/>24/7 Support <br/>Live Tracking</p>
        <Button className="ReadMore" variant="secondary"><Link to="/Services/VehicleMoving"   className="LinkItem">Read More</Link></Button>
       
      </div>
    </div>
  </div>
  </Col>
  <Col md={4} className="mb-3">
       <div class="cards">
     
    <div class="image"> 
      <img as={Image}  src={CargoService} height="300px" />
       <h3 class="bg-danger" className="CardText">Cargo Services</h3>

    </div>
    <div class="details"  width="auto">
      <div class="center"  width="auto">
     
        <h1>Cargo Services<br/></h1>
        <p>Rapid Movers and Packers Pakistan largest and trusted cargo services company, satisfying hundreds of customers daily.</p>
        <Button className="ReadMore" variant="secondary">Read More</Button>
        
      </div>
    </div>
  </div>
  </Col>
  <Col md={4} className="mb-3">
       <div class="cards">
    <div class="image">
      <img as={Image}  src={WareHouse} height="300px" />
      <h3 class="bg-danger" className="CardText">Warehouse and Storage</h3>

    </div>
    <div class="details"  width="auto">
      <div class="center"  width="auto">
        <h1>Warehouse and Storage Services<br/></h1>
        <p>Camera Security <br/>Professional Team<br/>24/7 Customer Support</p>
        <Button className="ReadMore" >Read More</Button>
        
      </div>
    </div>
  </div>
  </Col>
  <Col md={4} className="mb-3">
       <div class="cards">
    <div class="image">
      <img as={Image}  src={GoodsTransport} width="100%"  height="300px"/>
      <h3 class="bg-danger" className="CardText">Goods Transport</h3>

    </div>
    <div class="details"  width="auto">
      <div class="center"  width="auto">
        <h1>Goods Transport Company<br/></h1>
        <p>We offer full-fledged export and import services and transportation of goods for popular all ports.</p>
        <Button className="ReadMore" variant="secondary">Read More</Button>
       
      </div>
    </div>
  </div>
  </Col>
  <Col md={4} className="mb-3">
       <div class="cards">
    <div class="image">
      <img as={Image}  src={LuggageMoving} width="100%"  height="300px"/>
      <h3 class="bg-danger" className="CardText">Heavy Luggage</h3>

    </div>
    <div class="details"  width="auto">
      <div class="center"  width="auto">
        <h1>Heavy Luggage Moving<br/></h1>
        <p>Move your heavy luggage worldwide, store your luggage at cheap rates.</p>
        <Button className="ReadMore" variant="secondary">Read More</Button>
      
      </div>
    </div>
  </div>
  </Col>
  <Col md={4} className="mb-3">
       <div class="cards">
    <div class="image">
      <img as={Image}  src={CustomClearance} width="100%"  height="300px"/>
      <h3 class="bg-danger" className="CardText">Custom Clearance</h3>

    </div>
    <div class="details"  width="auto">
      <div class="center"  width="auto">
        <h1>Custom Clearance Agent<br/></h1>
        <p>Rapid Movers Lahore feels pleasure as one of the best customs clearance broker, freight forwarding, in Pakistan’s different cities.</p>
        <Button className="ReadMore" variant="secondary">Read More</Button>
      
      </div>
    </div>
  </div>
  </Col>
  <Col md={4}>
       <div class="cards">
    <div class="image">
      <img as={Image}  src={AirFreightService} width="100%"  height="300px"/>
      <h3 class="bg-danger" className="CardText">Air Freight Services</h3>

    </div>
    <div class="details"  width="auto">
      <div class="center"  width="auto">
        <h1>International Air Freight Services<br/></h1>
        <p>Airfreight means that moving bulky items to another city via plane</p>
        <Button className="ReadMore" variant="secondary">Read More</Button>
       
      </div>
    </div>
  </div>
  </Col>
  </Row>
  </Container>
    </div>
  )
}
export default Cards;