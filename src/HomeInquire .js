import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

const useStyles = makeStyles((theme) => ({
    formControl: {
      margin: theme.spacing(1),
      minWidth: 120,
    },
    selectEmpty: {
      marginTop: theme.spacing(2),
    },
  }));
  

export default function SimpleSelect() {
    const classes = useStyles();
    const [age, setAge] = React.useState('');
  
    const handleChange = (event) => {
      setAge(event.target.value);
    };
  
    return (
      <div>
        <FormControl className={classes.formControl}>
          <InputLabel id="demo-simple-select-label">Select Service</InputLabel>
          <Select
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            value={age}
            onChange={handleChange}
          >
            <MenuItem value={10}>Please select Service</MenuItem>
            <MenuItem value={210}>Home Shifting</MenuItem>
            <MenuItem value={30}>Celebrity Movers</MenuItem>
            <MenuItem value={210}>Office Moving</MenuItem>
            <MenuItem value={30}>Commercial Moving</MenuItem>
            <MenuItem value={210}>Vehicle Moving</MenuItem>
            <MenuItem value={30}>Cargo Services</MenuItem>
            <MenuItem value={210}>Warehouse Services</MenuItem>
            
          </Select>
        </FormControl>
        </div>
  );
}
