import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Navbar , Container , Nav , Brand ,NavDropdown, Collapse } from 'react-bootstrap'
import { BrowserRouter as Router ,Link, Route, Switch } from 'react-router-dom';
import Home from './Home';
import About from './About';
import Contact from './Contact';
import Slider from './Slider';
import HomeShifting from './Services/HomeShifting'
import OfficeMoving from './Services/OfficeMoving'
import VehicleMoving from './Services/VehicleMoving'
import Goods from './Services/GoodServices'
import Warehouse from './Services/Warehouse'
import AirService from './Services/AirService';
import './NavBar.css'
import Cargo from './Services/CargoService';
import HeavyLuggage from './Services/HeavyLuggage';
import Custom from './Services/Custom';
import Frequently from  './Frequently'
function Navbar1(){
return (
<div>
  <Router>
    <Navbar className="NavBar" expand="lg">
      <Container>
        <Navbar.Brand href="#home">React-Bootstrap</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
            <Nav.Link href="#home">
              <Link to="/" className="NavBar">
                Home
              </Link>
            </Nav.Link>

            <NavDropdown title="Dropdown">
              <NavDropdown.Item href="#action/3.1" className="NavBarDropdown">
                <Link to="/Services/HomeShifting">HOME SHIFTING</Link>
              </NavDropdown.Item>

              <NavDropdown.Item href="#action/3.2" className="NavBarDropdown">
                <Link to="/Services/OfficeMoving">Office Moving</Link>
              </NavDropdown.Item>
              <NavDropdown.Item href="#action/3.3" className="NavBarDropdown">
                <Link to="/Services/VehicleMoving">VEHICLE MOVING</Link>
              </NavDropdown.Item>
              <NavDropdown.Item href="#action/3.1" className="NavBarDropdown">
                <Link to="/Services/CargoService">Cargo Service</Link>
              </NavDropdown.Item>
              <NavDropdown.Item href="#action/3.2" className="NavBarDropdown">
                <Link to="/Services/Warehouse"> WAREHOUSE AND STORAGE </Link>
              </NavDropdown.Item>
              <NavDropdown.Item href="#action/3.3" className="NavBarDropdown">
                <Link to="/Services/GoodServices">Good Services</Link>
              </NavDropdown.Item>
              <NavDropdown.Item href="#action/3.1" className="NavBarDropdown">
                <Link to="/Services/HeavyLuggage">HEAVY LUGGAGE</Link>
              </NavDropdown.Item>
              <NavDropdown.Item href="#action/3.2" className="NavBarDropdown">
                <Link to="/Services/Custom">CUSTOM CLEARANCE</Link>
              </NavDropdown.Item>
              <NavDropdown.Item href="#action/3.3" className="NavBarDropdown">
              <Link to="/Services/AirService">  AIR FREIGHT SERVICES</Link>              
              </NavDropdown.Item>
            </NavDropdown>
            <Nav.Link href="#link">
              <Link to="/about" className="NavBar">
                ABOUT
              </Link>
            </Nav.Link>
            <Nav.Link>
              <Link to="/frequently" className="NavBar">
              FAQ'S
              </Link>
            </Nav.Link>
            <Nav.Link href="#pricing">
              <Link to="/contact" className="NavBar">
                Contact
              </Link>
            </Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>

    <Switch>
      <Route exact path="/">
        <Home />
      </Route>
      <Route path="/about">
        <About />
      </Route>
      <Route path="/Services/HomeShifting">
        <HomeShifting />
      </Route>
      <Route path="/Services/OfficeMoving">
        <OfficeMoving />
      </Route>
      <Route path="/Services/VehicleMoving">
        <VehicleMoving />
      </Route>
      <Route path="/Services/GoodServices">
        <Goods />
      </Route>
      <Route path="/Services/Warehouse">
        <Warehouse />
      </Route>
      <Route path="/Services/CargoService">
        <Cargo />
      </Route>
      <Route path="/Services/HeavyLuggage">
        <HeavyLuggage />
      </Route>
      <Route path="/Services/Custom">
        <Custom />
      </Route>
      <Route path="/Services/AirService">
        <AirService/>
      </Route>
      <Route path="/contact">
        <Contact />
      </Route>
    
      <Route path="/frequently">
        <Frequently/>
      </Route>
     
    </Switch>
  </Router>
</div>
  )
}
export default Navbar1;
