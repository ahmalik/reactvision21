import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import Home from './Home';
import Apps from './App.jsx'
import Navbar1 from './NavBar';
// import Home from './Home'
import { BrowserRouter as Router ,Route, Switch} from 'react-router-dom';


ReactDOM.render(
  <React.StrictMode>
    <Navbar1/> 

    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
