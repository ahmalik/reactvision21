import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Accordion from '@material-ui/core/Accordion';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import {Row,Col, Button,Image} from 'react-bootstrap'
import { Container } from '@material-ui/core';
import './Frequently.css'
import Wave from '../src/asset/image/svg.png'
import { Route, Router } from 'react-router-dom';
import FooterLinks from './FooterLink';
import FacebookIcon from '@material-ui/icons/Facebook';
import TwitterIcon from '@material-ui/icons/Twitter';
import InstagramIcon from '@material-ui/icons/Instagram';
import {TextField, FormControl , TextareaAutosize} from '@material-ui/core'



const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  heading: {
    fontSize: theme.typography.pxToRem(25),
    flexBasis: '30.33%',
    flexShrink: 0,
 
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(15),
    color: theme.palette.text.secondary,
  },
}));

export default function Frequently() {
    function  NewsLetters() {
        alert("SORRY :  This Service Is Unavailable At This Time")
      }
  const classes = useStyles();
  const [expanded, setExpanded] = React.useState(false);

  const handleChange = (panel) => (event, isExpanded) => {
    setExpanded(isExpanded ? panel : false);
  };

  return (
    <div className={classes.root}>
        <div>
        <img as={Image} src={Wave} width="100%" height="500px"/>
        </div>
        <Container>
        <Row> 
        <Col md={6}>
        <h1 className="Heading">General Information</h1>
      <Accordion expanded={expanded === 'panel1'} onChange={handleChange('panel1')} className="Accordion">
          
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1bh-content"
          id="panel1bh-header"
        >
          
          <Typography className={classes.secondaryHeading}  className="AccordionText">Why Should I choose Rapid Movers?</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography className="TypographyText">
          Rapid Movers and packers are proud to bring you peace of mind, and to be the brand you can trust for moving and storing needs. <br/>

We provide domestic, international travel services, both for residential and commercial purposes, and storage and disposal facilities for you. <br/>

On highest part of this one, we have experienced and skilled surveyors, prompt customer care response at your service.
          </Typography>
        </AccordionDetails>
      </Accordion>
      </Col>
      <Col md={6} >
          <h1  className="Heading">Services</h1>
      <Accordion expanded={expanded === 'panel13'} onChange={handleChange('panel13')} className="Accordion">
          
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel1bh-content"
            id="panel1bh-header"
          >
            
            <Typography className={classes.secondaryHeading}  className="AccordionText">Can you have disposal and storage services?</Typography>
          </AccordionSummary>
          <AccordionDetails>
            <Typography className="TypographyText">
            Yeah, both disposal and short and long-term storage facilities are offered.
            </Typography>
          </AccordionDetails>
        </Accordion>

      </Col>
      <Col md={6} className="mt-5">
      <Accordion expanded={expanded === 'panel2'} onChange={handleChange('panel2')} className="Accordion">
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel2bh-content"
          id="panel2bh-header"
        >
          <Typography className={classes.secondaryHeading} className="AccordionText"> 
          Do you provide service for moving on weekend / Holidays?
          </Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography className="TypographyText">
          Yes, however, travel services on weekends / Public Holidays will incur additional charges. So please contact for more details
          </Typography>
        </AccordionDetails>
      </Accordion>     
       </Col>
       <Col md={6} className="mt-5">
       <Accordion expanded={expanded === 'panel14'} onChange={handleChange('panel14')} className="Accordion">
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel2bh-content"
          id="panel2bh-header"
        >
          <Typography className={classes.secondaryHeading} className="AccordionText"> 
          Are you able to bring together the furniture parts?
          </Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography className="TypographyText">
          Yeah, our team is well trained to demolish or arrange different furniture, too.
          </Typography>
        </AccordionDetails>
      </Accordion>    
       </Col>
       
       <Col md={6} className="mt-5">
      <Accordion expanded={expanded === 'panel3'} onChange={handleChange('panel3')} className="Accordion">
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel3bh-content"
          id="panel3bh-header"
        >
          <Typography className={classes.secondaryHeading} className="AccordionText">
          Do you have a regular price plan?
          </Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography className="TypographyText">
          Unfortunately, we do not have a regular pricing plan. As our costs are based on the number of products you’re going to move and the situation varies.
          </Typography>
        </AccordionDetails>
      </Accordion></Col>
      <Col md={6} className="mt-5">
       <Accordion expanded={expanded === 'panel15'} onChange={handleChange('panel15')} className="Accordion">
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel2bh-content"
          id="panel2bh-header"
        >
          <Typography className={classes.secondaryHeading} className="AccordionText"> 
          How can I trust you to take care of / move my fragile items?
          </Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography className="TypographyText">
          Our men are licenced movers and packers, and we can use a range of protective materials to cover your delicate products safely before moving.
          </Typography>
        </AccordionDetails>
      </Accordion>    
       </Col>
      <Col md={6} className="mt-5">
      <Accordion expanded={expanded === 'panel4'} onChange={handleChange('panel4')} className="Accordion">
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel4bh-content"
          id="panel4bh-header"
        >
          <Typography className={classes.secondaryHeading} className="AccordionText">
          Do I have to pay for the survey?
          </Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography className="TypographyText">
          No, you ‘re not going to be needed to pay for the site survey. It is a free and non-compulsory service that we provide for a detailed quotation.
          </Typography>
        </AccordionDetails>
      </Accordion></Col>
      <Col md={6} className="mt-5">
      <Accordion expanded={expanded === 'panel16'} onChange={handleChange('panel16')} className="Accordion">
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel4bh-content"
          id="panel4bh-header"
        >
          <Typography className={classes.secondaryHeading} className="AccordionText">
          List of vehicle sizes / dimensions / estimations offered? </Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography className="TypographyText">
          10 ft Tailgate Exposed <br/>
Open 10 ft truck<br/> 
15 ft of truck with a tailgate<br/>
15 ft Truck Free of Tailgate<br/>
Twenty-four truck with tailgate<br/>
van
          </Typography>
        </AccordionDetails>
      </Accordion></Col>
      </Row>
      <Row> 
        <Col md={6}>

        <h1  className="Heading">Packing Material</h1>
      <Accordion expanded={expanded === 'panel5'} onChange={handleChange('panel5')} className="Accordion">
          
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1bh-content"
          id="panel1bh-header"
        >
          
          <Typography className={classes.secondaryHeading}  className="AccordionText">Size of Carton Boxes</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography className="TypographyText">
         1: Standard carton boxes: 20 inch by 16 inch by 16 inch <br/>
         2: Wardrobe boxes: 21.5 inch x 47 inch x 21.2 inch <br/>
        3:  Book carton boxes: 15 inch x 15 inch x 10 inch
          </Typography>
        </AccordionDetails>
      </Accordion>
      </Col>
      <Col md={6} className="mt-5">
      <Accordion expanded={expanded === 'panel17'} onChange={handleChange('panel17')} className="Accordion">
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel4bh-content"
          id="panel4bh-header"
        >
          <Typography className={classes.secondaryHeading} className="AccordionText">
          What's the quote for your package?   </Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography className="TypographyText">
          We tailored our packages to suit your needs. We would therefore strongly encourage you to contact our customer service department to arrange a site survey.
          </Typography>
        </AccordionDetails>
      </Accordion></Col>
      <Col md={6} className="mt-5">
          <h1  className="Heading">Moving Procedures</h1>
      <Accordion expanded={expanded === 'panel6'} onChange={handleChange('panel6')} className="Accordion">
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel2bh-content"
          id="panel2bh-header"
        >
          <Typography className={classes.secondaryHeading} className="AccordionText"> 
          May I travel sooner or later than the scheduled start time?
          </Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography className="TypographyText">
          It will rely on the situation, so we will recommend contacting our surveyor to inform you.
          </Typography>
        </AccordionDetails>
      </Accordion>     
       </Col>
       <Col md={6} className="mt-5">
      <Accordion expanded={expanded === 'panel18'} onChange={handleChange('panel18')} className="Accordion mt-5">
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel4bh-content"
          id="panel4bh-header"
        >
          <Typography className={classes.secondaryHeading} className="AccordionText">
          Will you supply the boxes and the packaging materials?   </Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography className="TypographyText ">
          Yeah, we are supplying boxes and packaging materials like packcing sheets and bubble paper  
          </Typography>
        </AccordionDetails>
      </Accordion></Col>
       
      <Col md={6} className="mt-5">
      <Accordion expanded={expanded === 'panel7'} onChange={handleChange('panel7')} className="Accordion" >
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel3bh-content"
          id="panel3bh-header"
        >
          <Typography className={classes.secondaryHeading} className="AccordionText">
          How long are the moves normally taking?
          </Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography className="TypographyText">
          It will depend on your number of products. Please feel free to contact our customer service to arrange a free and non-compulsory site survey.
          </Typography>
        </AccordionDetails>
      </Accordion></Col>
      <Col md={6}>

        <h1  className="Heading">Vehical</h1>
      <Accordion expanded={expanded === 'panel19'} onChange={handleChange('panel19')} className="Accordion">
          
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1bh-content"
          id="panel1bh-header"
        >
          
          <Typography className={classes.secondaryHeading}  className="AccordionText">Can I hire individual vehicles for other moving purposes?</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography className="TypographyText">
          Yes, you may hire our vehicles and move on your own. Drivers are included in the rental of vehicles.
          </Typography>
        </AccordionDetails>
      </Accordion>
      </Col>
      <Col md={6} className="mt-5">
      <Accordion expanded={expanded === 'panel8'} onChange={handleChange('panel8')} className="Accordion">
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel4bh-content"
          id="panel4bh-header"
        >
          <Typography className={classes.secondaryHeading} className="AccordionText">
          What happens, for example , in the case of an unexpected situation. Rain, raise down?
          </Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography className="TypographyText">
          It differs from one case to another. Our men will be able to give you guidance on the day.
          </Typography>
        </AccordionDetails>
      </Accordion></Col>
      <Col md={6} className="mt-5">


<Accordion expanded={expanded === 'panel20'} onChange={handleChange('panel20')} className="Accordion">
  
<AccordionSummary
  expandIcon={<ExpandMoreIcon />}
  aria-controls="panel1bh-content"
  id="panel1bh-header"
>
  
  <Typography className={classes.secondaryHeading}  className="AccordionText">What types of vehicles does Rapid Movers offer?</Typography>
</AccordionSummary>
<AccordionDetails>
  <Typography className="TypographyText">
  Yes, you may hire our vehicles and move on your own. Drivers are included in the rental of vehicles.
  </Typography>
</AccordionDetails>
</Accordion>
</Col>
      </Row>
      <Row> 
        <Col md={6}>

        <h1 className="Heading">Quotation Matters</h1>
      <Accordion expanded={expanded === 'panel9'} onChange={handleChange('panel9')} className="Accordion">
          
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1bh-content"
          id="panel1bh-header"
        >
          
          <Typography className={classes.secondaryHeading}  className="AccordionText">How do I get a quote from Rapid Movers?</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography className="TypographyText">
        You can select either a physical site survey or a contactless video call survey. Get your quote for Free & No Obligation right here.<br/>
If you’re seeing a full house change, we ‘re going to recommend that our customers opt for a physical site survey. Our surveyors are highly
 skilled and knowledgeable to identify items that may require special handling and will be able to provide a more comprehensive moving system.
          </Typography>
        </AccordionDetails>
      </Accordion>
      </Col>
   
      <Col md={6} className="mt-5">
      
      <Accordion expanded={expanded === 'panel10'} onChange={handleChange('panel10')} className="Accordion">
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel2bh-content"
          id="panel2bh-header"
        >
          <Typography className={classes.secondaryHeading} className="AccordionText"> 
          I've just got a few things to pass around. Hello, can I get a quote from you
          </Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography className="TypographyText">
          It’s certainly. We have a team that specialises in providing fast quotes for smaller tr
          ansfers. Just take some simple pictures of the things you want to pass around, send your moving information.
          </Typography>
        </AccordionDetails>
      </Accordion>     
       </Col>
       <Col md={6}></Col>
       
      
      <Col md={6}></Col>
      <Col md={6} className="mt-5">
      <Accordion expanded={expanded === 'panel12'} onChange={handleChange('panel12')} className="Accordion">
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel4bh-content"
          id="panel4bh-header"
        >
          <Typography className={classes.secondaryHeading} className="AccordionText">
          What happens, for example , in the case of an unexpected situation. Rain, raise down?
          </Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography className="TypographyText">
          It differs from one case to another. Our men will be able to give you guidance on the day.
          </Typography>
        </AccordionDetails>
      </Accordion></Col>
      <Col md={6} className="mt-5">
      <Accordion expanded={expanded === 'panel11'} onChange={handleChange('panel11')} className="Accordion">
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel3bh-content"
          id="panel3bh-header"
        >
          <Typography className={classes.secondaryHeading} className="AccordionText">
          How long are the moves normally taking?
          </Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography className="TypographyText">
          It will depend on your number of products. Please feel free to contact our customer service to arrange a free and non-compulsory site survey.
          </Typography>
        </AccordionDetails>
      </Accordion></Col>
      </Row>
      </Container>
      <footer className="Footer1 mt-3">
       <Container >
 <Row className="Footer">
   <Col md={3} ><Image src="https://ishfaqmovers.com/wp-content/uploads/2018/10/mo-white.png"  ></Image>
<h6 className="HomeFooterText">ISHFAQ MOVERS has been one of the leading packers &
   Movers in Karachi-Pakistan for more than 10 years. We deliver a broad range of targeted national and 
   international moving services for home, 
  house, villa, apartments, offices as well as relocation solutions for every purpose.</h6>
   </Col>
   <Col md={3} className="mt-3">
     <h3>QUICK LINKS</h3>


 
     <Route path="/about"><FooterLinks/></Route>
     <Route path="/contact"><FooterLinks/></Route>
   </Col>
   <Col md={3}  className="mt-3">
   <h3>OUR SERVICES</h3>

 
 <Route path="/about"><FooterLinks/></Route>
 <Route path="/contact"><FooterLinks/></Route>
   </Col>
   <Col md={3}  className="mt-2">
   <h3>NEWSLETTER SIGNUP</h3>
  <h6>No spam, unsubscribe at any time.</h6>
   <TextField id="standard-basic" label="Email" type="mail"  /><br/>
   <Button onClick={()=>NewsLetters()} className="HomeButton mt-2 ">Subscribe</Button>
   <br/>
   <h5   className="mt-2">Contact On Social Media</h5>
   <a href="#"><FacebookIcon></FacebookIcon></a>
   <a href="#"><TwitterIcon></TwitterIcon></a>
   <a href="#"><InstagramIcon></InstagramIcon></a>
 
   </Col>
 </Row>
       </Container>
      

       </footer>
    </div>
  );
}
