import React, { useRef, useState } from "react";
// Import Swiper React components
import { Swiper, SwiperSlide } from 'swiper/react';
// Import Swiper styles
import "swiper/swiper.min.css";
import "swiper/components/effect-coverflow/effect-coverflow.min.css"
import "swiper/components/pagination/pagination.min.css"
import "./Slider.css";
import Slider1 from './asset/image/Slider1.jpg'
import Slider2 from './asset/image/slider2.jpg'
import Slider3 from './asset/image/slider3.jpg'
import Slider4 from './asset/image/slider4.jpg'
import Slider5 from './asset/image/slider5.jpg'
import Slider6 from './asset/image/Slider6.jpg'
import Slider7 from './asset/image/Slider7.jpg'
import Slider8 from './asset/image/Slider8.jpg'
import Slider9 from './asset/image/Slider9.jpg'
// import Swiper core and required modules
import SwiperCore, { EffectCoverflow,Pagination} from 'swiper/core';
// install Swiper modules
SwiperCore.use([EffectCoverflow,Pagination]);

export default function Slider() {
  
  
  
  return (
    <>
    <Swiper effect={'coverflow'} grabCursor={true} centeredSlides={true} slidesPerView={'auto'} coverflowEffect={{

  "rotate": 50,
  "stretch": 0,
  "depth": 100,
  "modifier": 1,
  "slideShadows": false
}} pagination={false} className="mySwiper">
  <SwiperSlide><img as={Image}  src={Slider1} width="100% " height="400px"/></SwiperSlide>
  <SwiperSlide><img as={Image}  src={Slider2} width="100% " height="400px" /></SwiperSlide>
  <SwiperSlide><img as={Image}  src={Slider3} width="100% " height="400px" /></SwiperSlide>
  <SwiperSlide><img as={Image}  src={Slider4} width="100% " height="400px" /></SwiperSlide>
  <SwiperSlide><img as={Image}  src={Slider5} width="100% " height="400px" /></SwiperSlide>
  <SwiperSlide><img as={Image}  src={Slider6} width="100% " height="400px" /></SwiperSlide>
  <SwiperSlide><img as={Image}  src={Slider7} width="100% " height="400px" /></SwiperSlide>
  <SwiperSlide><img as={Image}  src={Slider8} width="100% " height="400px" /></SwiperSlide>
  <SwiperSlide><img as={Image}  src={Slider9} width="100% " height="400px" /></SwiperSlide>
  </Swiper>
    </>
  )
}