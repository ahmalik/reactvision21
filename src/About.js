import {Container , Button ,Row,Col , Image} from 'react-bootstrap'
import React from 'react';
import {TextField, FormControl , TextareaAutosize} from '@material-ui/core'
import AboutHeader from '../src/asset/image/AboutHeader.jpg'
import Service24 from '../src/asset/image/SERVICE24.png'
import SafeVehicles from '../src/asset/image/SAFEVEHICLES.png'
import LiveChat from '../src/asset/image/LiveChat.png'
import World from '../src/asset/image/World.png'
import Delivery from '../src/asset/image/Delivery.png'
import Team from '../src/asset/image/Team.png'
import About1 from '../src/asset/image/About.png'
import { Route, Router } from 'react-router-dom';
import FooterLinks from './FooterLink';
import FacebookIcon from '@material-ui/icons/Facebook';
import TwitterIcon from '@material-ui/icons/Twitter';
import InstagramIcon from '@material-ui/icons/Instagram';

import './About.css'
import Frequently from './Frequently';


function About(){
  function  NewsLetters() {
    alert("SORRY :  This Service Is Unavailable At This Time")
  }
    return(
      <div style={{ backgroundColor: "	 #eee2e2" }}>
      <div>
   
   <img as={Image} src={AboutHeader} className="AboutHeaderImage"  width="100%" height="500px"/>
  <h1 className="AboutHeaderText">About Us</h1>

      </div>
      <Container>
        <h1 className="text-center  AboutServiceHeading ">Company Profile</h1>
        <p className="AboutServiceText">Rapid Movers is a packing and moving company that provides time and cost efficient services throughout Pakistan. We have a team of expert packers and movers who have years of experience in the field. Our company guarantees quality and variety when it comes to our array of services.</p>
        <Row>
      <Col md={6}>
        <h1 className="text-center  AboutServiceHeading">Vision</h1>
        <p className="text-center  AboutServiceText">We aim to make our packing and moving services accessible to everyone in the country. Our services are essential in making any moving experience run smoothly as possible.
           The Rapid Movers team aims to optimize customer satisfaction when it comes to any moving job.</p>
      </Col>
      <Col md={6}>
        <h1 className="text-center AboutServiceHeading ">Mission</h1>
        <p className="text-center AboutServiceText ">Equipped with the latest equipment and a skilled team of professionals, our mission is to provide clients with a customizable, safe, affordable and punctual moving experience. Our team will provide you with the services to make you feel completely comfortable during your move.</p>
      </Col>
        </Row>
        <Row>
          <Col md={8}>
            <Row>
            <Col md={6}>
                 <img as={Image} src={Service24} className="AboutServiceImage"  />

        <h1 className="text-center AboutServiceHeading">24/4 SERVICE</h1>
        <p className="text-center AboutServiceText">“Our dedicated team works long and hard to make sure that your move is completed by the deadline you set. They will carefully work day and night to complete your moving job, no matter how difficult it is.”</p>
      </Col>
      <Col md={6}>
      <img as={Image} src={SafeVehicles} className="AboutServiceImage"  style={{ opacity: 0.6 }}/>
        <h1 className="text-center AboutServiceHeading ">SAFE VEHICLES</h1>
        <p className="text-center AboutServiceText ">“Our moving vehicles are always safe and well-maintained. There is no need to fret because at Rapid Movers, we make sure to always check our moving trucks before our team sets out for a job.”</p>
      </Col>
      <Col md={6}>
                 <img as={Image} src={LiveChat} className="AboutServiceImage" style={{ opacity: 0.6 }} />

        <h1 className="text-center AboutServiceHeading  ">24/4 SERVICE</h1>
        <p className="text-center AboutServiceText  ">“Our dedicated team works long and hard to make sure that your move is completed by the deadline you set. They will carefully work day and night to complete your moving job, no matter how difficult it is.”</p>
      </Col>
      <Col md={6}>
      <img as={Image} src={World} className="AboutServiceImage"  />
        <h1 className="text-center AboutServiceHeading">GLOBAL SERVICES</h1>
        <p className="text-center AboutServiceText">“Rapid Movers provides services that are of a global standard.
         Our services are available all over Pakistan and we even handle customs services.”</p>
      </Col>
      <Col md={6}>
      <img as={Image} src={Delivery} className="AboutServiceImage"  style={{ opacity: 0.6 }} />
        <h1 className="text-center AboutServiceHeading ">ON TIME DELIVERY</h1>
        <p className="text-center  AboutServiceText">“One of the most standout qualities of Rapid Movers is our team’s punctuality. We always provide on-time delivery services from doorstep to doorstep and we always take the client’s deadlines into consideration.”</p>
      </Col>
      <Col md={6}>
      <img as={Image} src={Team} className="AboutServiceImage"  style={{ opacity: 0.6 }}/>
        <h1 className="text-center AboutServiceHeading ">PROFESSIONAL TEAM</h1>
        <p className="text-center AboutServiceText ">“Our professional team consists of people with years of experience in their various roles. Our professionals consist of drivers, packers, loaders, logistics managers and more!”</p>
      </Col>
      
            </Row>
        
          </Col>
          <Col md={4}>
          <img as={Image} src={About1} className="mt-5" width="100%" />
          </Col>
        </Row>
      </Container>
      <footer className="Footer1">
       <Container >
 <Row className="Footer">
   <Col md={3} ><Image src="https://ishfaqmovers.com/wp-content/uploads/2018/10/mo-white.png"  ></Image>
<h6 className="HomeFooterText">ISHFAQ MOVERS has been one of the leading packers &
   Movers in Karachi-Pakistan for more than 10 years. We deliver a broad range of targeted national and 
   international moving services for home, 
  house, villa, apartments, offices as well as relocation solutions for every purpose.</h6>
   </Col>
   <Col md={3} className="mt-3">
     <h3>QUICK LINKS</h3>


 
     <Route path="/about"><FooterLinks/></Route>
     <Route path="/contact"><FooterLinks/></Route>
   </Col>
   <Col md={3}  className="mt-3">
   <h3>OUR SERVICES</h3>

 
 <Route path="/about"><FooterLinks/></Route>
 <Route path="/contact"><FooterLinks/></Route>
   </Col>
   <Col md={3}  className="mt-2">
   <h3>NEWSLETTER SIGNUP</h3>
  <h6>No spam, unsubscribe at any time.</h6>
   <TextField id="standard-basic" label="Email" type="mail"  /><br/>
   <Button onClick={()=>NewsLetters()} className="HomeButton mt-2 ">Subscribe</Button>
   <br/>
   <h5   className="mt-2">Contact On Social Media</h5>
   <a href="#"><FacebookIcon></FacebookIcon></a>
   <a href="#"><TwitterIcon></TwitterIcon></a>
   <a href="#"><InstagramIcon></InstagramIcon></a>
 
   </Col>
 </Row>
       </Container>
      

       </footer>
    
 
      </div>
    )
  }
  
  export default About;