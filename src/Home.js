import React from 'react';
import Slider from './Slider';
import './Home.css';
import {Container , Button ,Card ,Row,Col , Image} from 'react-bootstrap'
import {TextField, FormControl , TextareaAutosize} from '@material-ui/core'
import SimpleSelect from './HomeInquire '
import { Route, Router } from 'react-router-dom';
import FooterLinks from './FooterLink';
import Cards from './Card';
import OurClient from './OurClient'
import FacebookIcon from '@material-ui/icons/Facebook';
import TwitterIcon from '@material-ui/icons/Twitter';
import InstagramIcon from '@material-ui/icons/Instagram';
function Home(){
  function Qoute(){
  alert("SORRY :  This Service Is Unavailable At This Time")
  }
  function  NewsLetters() {
    alert("SORRY :  This Service Is Unavailable At This Time")
  }
    return(
      <div style={{backgroundColor:"	 #eee2e2"}} >

<Container>
  <Slider/>
<div  className="HomeWelcome" >
        <h1 >WELCOME TO ISHFAQ MOVERS IN KARACHI</h1>
        <p className="HomeHeading">Firstly, We are Pakistan's No.1 Packers And Movers In Karachi - Pakistan.
            Secondly, Ishfaq movers in Karachi is one of the surpassing Goods Transporter, International, and local
            cargo movers and packers in Pakistan. It’s our family business and it’s been in the operation for four
            generations up to this point. We Follow the Movers Guideline in our every move. In our movements, we have
            always succeeded in winning our clients’ hearts by catering to them with our fine services. Above all, Our
            mission is to serve people with hardships. In Conclusion, We are experts in shifting and relocating your
            belongings with the care and attention that they need. We’ve had people who would accomplish our daily tasks
            with their experience in the field, now we have educated people who are even more qualified to perform such
            actions.</p>
        <Button   className="HomeButton">MORE ABOUT US</Button> &nbsp;&nbsp;
        <Button  onClick={()=>Qoute()} className="HomeButton">REQUEST A QOUTE</Button>
    </div></Container>

 
 <Container>
      
    <h1 className="p-1">ISHFAQ MOVERS IN KARACHI SERVICES</h1>
    <Row className="mt-3">
  

  <Cards/>
  </Row>

    </Container>
    <hr/>
    <Container >
  
      <Row>
  <Col md={12}>  <h1 className="HomeHeading1">WHY ISHFAQ MOVERS</h1></Col>
        <Col md={6}>
      
                <h4 className="HomeHeading2">AFFORDABLE YET HIGH-QUALITY MOVING SERVICES</h4>
                <p className="HomePara">We watch our costs very closely and have become very efficient during our 20 years as a moving
                    company. When we save money, we pass that along to our customers in the form of lower overall moving
                    costs.</p>
                <h4 className="HomeHeading2">WE HANDLE EVERYTHING FOR YOUR MOVE</h4>
                <p className="HomePara">Ishfaq movers offers all moving services that relate to moving including local moves, long distance
                    moves, packing and unpacking, commercial moves and insurance (through third parties) for any damage
                    that might happen. Which means we will take care of your move from a through z, and you don’t need
                    to be concerned anymore.</p>
                <h4 className="HomeHeading2">PROFESSIONAL AND PERSONAL CUSTOMER SUPPORT</h4>
                <p className="HomePara">We cultivate a friendly attitude by treating our customers with respect and decency. We acknowledge
                    that fact that what truly matters is having a healthy and strong relationship with all of our
                    customers.</p>
                <h4 className="HomeHeading2">EXTRA CARE
                </h4>
                <p className="HomePara">We value your possessions as we value ours. And we have the tiny knowledge that nobody would want any
                    of their item to be lost or be broken. That’s why we give extra supervision and care to your items.
                </p>
        </Col>
        <Col md={6 }>

        <h1 className="HomeHeading2">REQUEST A QUICK QUOTE</h1>
                        <br/>
                        <p className="HomePara">To inquire about our services further kindly submit the form below, We'd be as quick as
                            possible to help you out.</p>
                            <Container> 
                            <form >
                              
                              <Row>
                                <Col md={5}><TextField id="standard-basic" label="First Name" type="text"/></Col>
                                <Col md={5}><TextField id="standard-basic" label="Last Name" className="mb-3" /></Col>
                              
                                <Col md={5}><TextField id="standard-basic" label="Email" type="mail" /></Col>
                                <Col md={5}><TextField id="standard-basic" label="Phone Number" type="text"className="mb-3" /></Col>
                                <Col md={5} className="mb-3"><SimpleSelect /></Col>
                                </Row>
                                <Row>
                                  <Col md={5}><Button type="submit" className="HomeButton1" >Submit Form</Button></Col>
                                  <Col md={5}><Button type="reset" className="HomeButton1">Reset Form</Button></Col>
                                </Row>
                              
                            </form>
                             </Container> 

        </Col>
      </Row>
    </Container>   <hr/> 
    
    
    <Container className="mb-1"> 
  <OurClient/>
       </Container>
       <footer className="Footer1">
       <Container >
 <Row className="Footer">
   <Col md={3} ><Image src="https://ishfaqmovers.com/wp-content/uploads/2018/10/mo-white.png"  ></Image>
<h6 className="HomeFooterText">ISHFAQ MOVERS has been one of the leading packers &
   Movers in Karachi-Pakistan for more than 10 years. We deliver a broad range of targeted national and 
   international moving services for home, 
  house, villa, apartments, offices as well as relocation solutions for every purpose.</h6>
   </Col>
   <Col md={3} className="mt-3">
     <h3>QUICK LINKS</h3>

     <p><FooterLinks/></p>
 
     <Route path="/about"><FooterLinks/></Route>
     <Route path="/contact"><FooterLinks/></Route>
   </Col>
   <Col md={3}  className="mt-3">
   <h3>OUR SERVICES</h3>
   <p><FooterLinks/></p>
 
 <Route path="/about"><FooterLinks/></Route>
 <Route path="/contact"><FooterLinks/></Route>
   </Col>
   <Col md={3}  className="mt-2">
   <h3>NEWSLETTER SIGNUP</h3>
  <h6>No spam, unsubscribe at any time.</h6>
   <TextField id="standard-basic" label="Email" type="mail"  /><br/>
   <Button onClick={()=>NewsLetters()} className="HomeButton mt-2 ">Subscribe</Button>
   <br/>
   <h5   className="mt-2">Contact On Social Media</h5>
   <a href="#"><FacebookIcon></FacebookIcon></a>
   <a href="#"><TwitterIcon></TwitterIcon></a>
   <a href="#"><InstagramIcon></InstagramIcon></a>
 
   </Col>
 </Row>
       </Container>
       <hr/>

       </footer>
      </div>
     
    )
  }

  export default Home;