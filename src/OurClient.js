import {Row,Col , Image} from 'react-bootstrap'
import ourClient1 from './asset/image/ourclient1.png';
import ourClient2 from './asset/image/ourclient2.png'
import ourClient3 from './asset/image/ourclient3.png'
import ourClient4 from './asset/image/ourclient4.png'
import ourClient5 from './asset/image/ourclient5.png'
import ourClient6 from './asset/image/ourclient6.png'
import ourClient7 from './asset/image/ourclient7.png'
import ourClient8 from './asset/image/ourclient8.png'
import ourClient9 from './asset/image/ourclient9.png'
import ourClient10 from './asset/image/ourclient10.png'
import ourClient11 from './asset/image/ourclient11.png'
import ourClient12 from './asset/image/ourclient12.png'
import './OurClient.css'
function OurClient(){

    return(
        <div>
                <h1 className="OurClientHeading">OUR CLIENTS</h1>

<Row className="p-1">
  <Col md={2}><Image as={Image}  src={ourClient1} rounded  width="100%" height="175px"  className="imageHover p-1 m-1" /></Col>
  <Col md={2}><Image as={Image}  src={ourClient2} rounded  width="100%" height="175px" className="imageHover p-1 m-1"/></Col>
  <Col md={2}><Image as={Image}  src={ourClient3} rounded  width="100%" height="175px" className="imageHover p-1 m-1"/></Col>
  <Col md={2}><Image as={Image}  src={ourClient4} rounded  width="100%" height="175px" className="imageHover p-1 m-1"/></Col>
  <Col md={2}><Image as={Image}  src={ourClient5} rounded  width="100%" height="175px" className="imageHover p-1 m-1"/></Col>
  <Col md={2}><Image as={Image}  src={ourClient6} rounded  width="100%" height="175px" className="imageHover p-1 m-1"/></Col>
</Row >
<Row className="mt-2 p-1 mb-3"><br/>
  <Col md={2}><Image as={Image}  src={ourClient7} rounded  width="100%" height="175px" className="imageHover p-1 m-1"/></Col>
  <Col md={2}><Image as={Image}  src={ourClient8} rounded  width="100%" height="175px" className="imageHover p-1 m-1"/></Col>
  <Col md={2}><Image as={Image}  src={ourClient9} rounded  width="100%" height="175px" className="imageHover p-1 m-1"/></Col>
  <Col md={2}><Image as={Image}  src={ourClient10} rounded  width="100%" height="175px" className="imageHover p-1 m-1"/></Col>
  <Col md={2}><Image as={Image}  src={ourClient11} rounded  width="100%" height="175px" className="imageHover p-1 m-1"/></Col>
  <Col md={2}><Image as={Image}  src={ourClient12} rounded  width="100%" height="175px" className="imageHover p-1 m-1"/></Col>
</Row>
        </div>
    )
}
export default OurClient;